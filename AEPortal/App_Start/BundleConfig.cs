﻿using System.Web.Optimization;

namespace AEPortal {
    public class BundleConfig {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles) {
            bundles.Add(new ScriptBundle("~/bundles/Js").Include("~/Scripts/jquery-2.2.3.js",
				"~/Scripts/jquery-ui-1.11.4.js",
				"~/Scripts/jquery.datetimepicker.js",
				"~/Scripts/moment.js",
				"~/Scripts/select2.js",
				"~/Scripts/bootstrap.js",
				"~/Scripts/angular.js",
				"~/Scripts/ui-grid.js",
				"~/Scripts/angular-ui/ui-bootstrap-tpls.js",
				"~/Scripts/generalHelpers.js"));
            //"~/Scripts/multiselect.js"

            bundles.Add(new StyleBundle("~/Content/Css").Include("~/Content/bootstrap.css",
				"~/Content/bootstrap-theme.css",
				"~/Content/jquery-ui.css",
				"~/Content/jquery.datetimepicker.css",
				"~/Content/select2.css",
				"~/Content/ui-grid.css"));

            bundles.Add(new ScriptBundle("~/bundles/Manifest/js").Include("~/Manifest/Scripts/*.js"));
            bundles.Add(new StyleBundle("~/Content/Manifest/css").Include("~/Manifest/Content/*.css"));
			bundles.Add(new ScriptBundle("~/bundles/SupplierPortal/js").Include("~/SupplierPortal/Scripts/*.js"));
			bundles.Add(new StyleBundle("~/bundles/SupplierPortal/css").Include("~/SupplierPortal/Content/*.css"));


		}
	}
}