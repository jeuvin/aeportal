﻿using System.Web.Mvc;

namespace AEPortal.Controllers {
	public class DefaultController : Controller {
        // GET: Default
        public ActionResult Index() {
            return RedirectToAction("Index", "Tasks", new {area = "Manifest"});
        }
    }
}