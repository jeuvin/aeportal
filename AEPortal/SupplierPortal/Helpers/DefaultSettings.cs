﻿using System;
using System.Collections.Generic;
using System.Web.Helpers;
using SICommon.Enums;
using SICommon.Models;
using SICommon.SPModels;

namespace AEPortal.SupplierPortal.Helpers {
	internal static class DefaultSettings {
    public const string DEFAULT_SEARCH_NAME = "Pending Requests";

    private static readonly GridSettings defaultGridSettings = new GridSettings {
      Columns = new List<string> {"PickupDateTime", "PickupLocation", "CarGroup", "VoucherNumber", "SupplierConfirmation", "LastName"},
      SortColumn = "PickupDateTime",
      SortDirection = SortDirection.Ascending
    };

    private static readonly GridSettings confirmedGridSettings = new GridSettings {
      Columns = new List<string> {"ConfirmedDateTime", "PickupLocation", "CarGroup", "VoucherNumber", "SupplierConfirmation", "LastName"},
      SortColumn = "ConfirmedDateTime",
      SortDirection = SortDirection.Descending
    };

    public static readonly Dictionary<RequestStatus, GridSettings> requestStatusToGridSettings = new Dictionary<RequestStatus, GridSettings> {
      {RequestStatus.All, defaultGridSettings},
      {RequestStatus.Confirmed, confirmedGridSettings},
      {RequestStatus.Denied, confirmedGridSettings},
      {RequestStatus.Obsolete, defaultGridSettings},
      {RequestStatus.Pending, defaultGridSettings}
    };

    public static readonly Dictionary<string, SearchParameters> namedSearches = new Dictionary<string, SearchParameters> {
      {
        DEFAULT_SEARCH_NAME, new SearchParameters {
          RequestStatus = RequestStatus.Pending,
          VoucherStatus = VoucherStatus.All
        }
      }, {
        "Last Minute Requests", new SearchParameters {
          PickupDateLowerBound = DateTime.UtcNow.AddDays( -1 ),
          PickupDateUpperBound = DateTime.UtcNow.AddDays( 3 ),
          RequestStatus = RequestStatus.Pending,
          VoucherStatus = VoucherStatus.All
        }
      }, {
        "Upcoming Reservations", new SearchParameters {
          PickupDateLowerBound = DateTime.UtcNow.AddDays( -1 ),
          PickupDateUpperBound = DateTime.UtcNow.AddDays( 7 ),
          RequestStatus = RequestStatus.Confirmed,
          VoucherStatus = VoucherStatus.Confirmed
        }
      }, {
        "Confirmed Reservations", new SearchParameters {
          CreatedDateLowerBound = DateTime.UtcNow.AddDays( -40 ),
          CreatedDateUpperBound = DateTime.UtcNow.AddDays( 1 ),
          RequestStatus = RequestStatus.Confirmed,
          VoucherStatus = VoucherStatus.Confirmed
        }
      }, {
        "Denied Reservations", new SearchParameters {
          RequestStatus = RequestStatus.Denied,
          VoucherStatus = VoucherStatus.Denied
        }
      }, {
        "Canceled Reservations", new SearchParameters {
          RequestStatus = RequestStatus.Confirmed,
          VoucherStatus = VoucherStatus.Canceled
        }
      }
    };
  }
}