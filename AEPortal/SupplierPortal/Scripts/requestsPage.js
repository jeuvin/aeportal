﻿var requestApp = angular.module('requestApp', ['SPServices', 'ui.bootstrap']);
requestApp.controller('RequestController', ['$scope', '$http', 'SharedServices', function ($scope, $http, SharedServices) {
    // Voucher Length
    $scope.voucherSystem = {
        AU:7,
        CA:7,
        EU:8,
        HO:6,
        KE:6,
        LA:6,
        NZ:6,
        UK:7,
        US:7,
        ZA:5
    };
    var dateFormat = "dd M yy";
    var altDateFormat = "mm/dd/yy 00:00:00";
    var currentGridQuery = "";
    var currentSearchName = "Pending Requests";
    var pageParameters = {
        lastRefreshDateTime: null,
        row: 0,
        rowsPerPage: 20,
        searchParameters: {
            supplierId: 0,
            requestStatus: "All",
            voucherStatus: "All",
            carGroup: "",
            confirmationNumber: "",
            dropoffLocations: [],
            pickupLocations: [],
            firstName: "",
            lastName: "",
            systemCode: "",
            voucherNumber: "",
            createdDateLowerBound: "",
            createdDateUpperBound: "",
            dropoffDateLowerBound: "",
            dropoffDateUpperBound: "",
            pickupDateLowerBound: "",
            pickupDateUpperBound: ""
        }
    };
    var pageTitle = document.title;

    $(document).ready(function () {
        if ($("#grid").length === 0) {
            return;
        }
        // Setup Datepickers
        $("#pickupStartDate").datepicker({
            dateFormat: dateFormat
        });
        $("#pickupEndDate").datepicker({
            dateFormat: dateFormat
        });
        $("#dropoffStartDate").datepicker({
            dateFormat: dateFormat
        });
        $("#dropoffEndDate").datepicker({
            dateFormat: dateFormat
        });
        $("#createdStartDate").datepicker({
            dateFormat: dateFormat
        });
        $("#createdEndDate").datepicker({
            dateFormat: dateFormat
        });

        pageParameters.searchParameters.supplierId = $("#chooseSupplier").length != 0 ? $("#chooseSupplier").val() : 0;
        pageParameters.searchParameters.requestStatus = $("#requestStatus .active input").prop("id");
        pageParameters.searchParameters.voucherStatus = $("#voucherStatus .active input").prop("id");
        setSearchParmeters();
        currentSearchName = $("#quickSearch li.active a").text();

        setArrows();
    });

    function setSearchParmeters() {
        pageParameters.searchParameters.systemCode = $("#voucherNumber").val().substring(0, 2);
        pageParameters.searchParameters.voucherNumber = $("#voucherNumber").val().substring(2);
        pageParameters.searchParameters.confirmationNumber = $("#reservationNumber").val();
        pageParameters.searchParameters.firstName = $("#firstName").val();
        pageParameters.searchParameters.lastName = $("#lastName").val();
        pageParameters.searchParameters.createdDateLowerBound = formatDatetoDateTime($("#createdStartDate").val(), dateFormat);
        pageParameters.searchParameters.createdDateUpperBound = formatDatetoDateTime($("#createdEndDate").val(), dateFormat);
        pageParameters.searchParameters.dropoffDateLowerBound = formatDatetoDateTime($("#dropoffStartDate").val(), dateFormat);
        pageParameters.searchParameters.dropoffDateUpperBound = formatDatetoDateTime($("#dropoffEndDate").val(), dateFormat);
        pageParameters.searchParameters.pickupDateLowerBound = formatDatetoDateTime($("#pickupStartDate").val(), dateFormat);
        pageParameters.searchParameters.pickupDateUpperBound = formatDatetoDateTime($("#pickupEndDate").val(), dateFormat);
    };

    // Search and Filter box events

    $("#expandSearchAndFilterBtn").ready(function () {
        $('#expandSearchAndFilterBtn').attr("title", "Expand custom search options");
    });

    $(document).on("click", "#expandSearchAndFilterBtn", function () {
        if ($('#SearchAndFilterControlDiv').is(':visible')) {
            $('#expandSearchAndFilterBtn').attr("title", "Expand custom search options");
        } else {
            $('#expandSearchAndFilterBtn').attr("title", "Close custom search options");
        }

        $("#SearchAndFilterControlDiv").toggle();
        $("#btnIcon").toggleClass("glyphicon-chevron-down glyphicon-chevron-up");
    });

    // Export Events
    $(document).on("click", "#export li", function () {
        var format = $($(this).children()[0]).text();
        $scope.buildFileForExport(format);
    });

    $("grid").ready(function () {
        $('#grid th').each(function () { $(this).attr('title', "Sort by " + $(this).text()); });
        $('#grid tr').each(function () { $(this).attr("title", "Click to see request history and details"); });
    });

    function setArrows() {
        var dir = $('#dir').val();
        var col = $('#col').val().substr($('#col').val().indexOf('.') + 1, $('#col').val().length);
        if (col == "") {
            col = "PickupDateTime";
        }
        var header = $('th a[href*=' + col + ']');
        if (dir == 'Ascending') {
            header.text(header.text() + ' ▼');
        }
        if (dir == 'Descending') {
            header.text(header.text() + ' ▲');
        }
    }
    
    // Angular Post methods
    $scope.buildFileForExport = function (format) {
        var data = { "searchParameters": pageParameters.searchParameters, "format": format };
        
        if (pageParameters.searchParameters.supplierId != 0) {
            data.supplierId = pageParameters.searchParameters.supplierId;
        }
	
        $http.post(getCurrentURL() + "/SupplierPortal/Requests/BuildFileForExport/", data)
            .success(function (cacheKey) {
                if (cacheKey == null || cacheKey === "") {
                    $("#messageModal .modal-body h3").text("No requests found to export");
                    $("#messageModal").modal();
                } else {
                    var url = getCurrentURL() + "/SupplierPortal/Requests/GetFileForExport/?cacheKey=" + cacheKey + "&format=" +format;
                    window.location = url;
                }
            }).error(function (error, status) {
                statusCodeFunctions();
            });
    };
    
    $scope.doFreeTextSearch = function (text, supplierId) {
        $("#voucherDetails").empty();
        var data = { "text": text, "SupplierId": supplierId};

        $http.post(getCurrentURL() + "/SupplierPortal/Requests/DoFreeTextSearch/", data)
            .success(function (data) {
                if (data != null) {
                    $("#grid").html($(data + " .webgrid-table").html());
                    setArrows();
                }
                document.title = pageTitle;
            }).
            error(function (error, status) {
                onAjaxError();
                statusCodeFunctions(status);
            }
        );
    }

    $scope.loadNewGrid = function (checkNewRequests) {
        $("#voucherDetails").empty();
        if (checkNewRequests == undefined) {
            checkNewRequests = false;
        }
        var queryString = currentGridQuery === "" ? "" : "?" + currentGridQuery;
        $http.post(getCurrentURL() + "/SupplierPortal/Requests/GetGrid/" +queryString, pageParameters)
        .success(function (data) {
            $("#grid").html($(data + " #grid").html());

            if (checkNewRequests) {
                var newRows = $("#newRequestsCount").val();
                if (newRows > 0) {
                    document.title = "(" + newRows + ") " + pageTitle;
                } else {
                    document.title = pageTitle;
                }
            } else {
                pageParameters.lastRefreshDateTime = $("#lastRefreshDateTime").val();
                document.title = pageTitle;
            }
            $("#freeTextSearch").val("");
            setArrows();
        }).error(function (error, status) {
            checkNewRequests ? function () { } : onAjaxError();
            statusCodeFunctions(status);
        });
    };

    $scope.loadRequestDetails = function (voucherNumber, requestId) {
        var urlWithParams = getCurrentURL() + "/SupplierPortal/Requests/GetVoucherDetails?voucherNumber=" + voucherNumber + "&requestId=" + requestId;
        $http.post(urlWithParams)
            .success(function (data) {
                $("#voucherDetails").html($(data));
            }).error(function (error, status) {
                onAjaxError(status);
                statusCodeFunctions(status);
            });
    };

    // End Post methods 
    
    // Search events
    
    $(document).on("keyup", "#freeTextSearch", function () {
        var SystemRgx = "[a-zA-Z]{2}";
        var VouchRgx = "[0-9]*$";
        var system = $(this).val().match(SystemRgx);
        var VouchNumber = $(this).val().match(VouchRgx)[0];
        var textVal = this;


        if (system && VouchNumber) {
            if (system[0].length === 2 && VouchNumber.length >= $scope.voucherSystem[system[0].toUpperCase()]) {
                setTimeout(function () {
                    $scope.doFreeTextSearch(($(textVal).val()), $('#chooseSupplier option:selected')[0].value);
                }, 2000);
            }
        } else if ($(this).val() === "")
            $scope.refreshGrid();
    });

    $(document).on('click', '#grid tbody tr', function () {
        //Remove the selectedRow class from a previously selected row
        $("#grid tbody tr").each(function () {
            if ($(this).hasClass("selectedRow")) {
                $(this).removeClass("selectedRow");
            }
        });
        //Freeze the background color on the currently selected row
        $(this).addClass("selectedRow");
        var tds = $(this).find('td');
        $("#gridRowIndex").val(tds.context.rowIndex);
        var i = 0;
        var voucherIndex = 0;
        $("#grid thead th").each(function () {
            if ($(this).find('a').text().indexOf("Voucher Number") == 0) {
                voucherIndex = i;
                return false;
            }
            i += 1;
        });
        $scope.loadRequestDetails(tds[voucherIndex].innerHTML, tds[0].innerHTML);

    });

    $(document).on("click", "#requestLinksDiv a", function () {
        var parameters = this.id.split("-");
        $scope.loadRequestDetails(parameters[0], parameters[1]);
    });

    $(document).on("click", "#requestStatus .btn", function () {
        pageParameters.searchParameters.requestStatus = $(this).children().prop("id");
        $scope.loadNewGrid();
    });
    
    $(document).on("click", "#voucherStatus .btn", function () {
        pageParameters.searchParameters.voucherStatus = $(this).children().prop("id");
        $scope.loadNewGrid();
    });

    // Setup page refresh 
    $scope.refreshGrid = function () {
        pageParameters.lastRefreshDateTime = pageParameters.lastRefreshDateTime == null ? $("#lastRefreshDateTime").val() : pageParameters.lastRefreshDateTime;
        if ($("#voucherDetails").children().length == 0 && $("#freeTextSearch").val().length < 3) {
            $scope.loadNewGrid(true);
        }
    }

    $(document).on("click", "#searchBtnDiv", function () {
        setSearchParmeters();
        $scope.loadNewGrid();
    });

    $(document).on("change", "#CarGroupFilter", function () {
        var selectedElement = $(this).find(":selected");
        pageParameters.searchParameters.carGroup = selectedElement.index() != 0 ? selectedElement.text() : "";
        $scope.loadNewGrid();
    });

    $(document).on("change", "#DropoffLocationFilter", function () {
        var selectedLocations = $(this).val();
        pageParameters.searchParameters.dropoffLocations = selectedLocations == null ? [] : selectedLocations;
        $scope.loadNewGrid();
    });

    $(document).on("change", "#PickupLocationFilter", function () {
        var selectedLocations = $(this).val();
        pageParameters.searchParameters.pickupLocations = selectedLocations == null ? [] : selectedLocations;
        $scope.loadNewGrid();
    });

    // Grid Events
    $(document).on("click", "#grid .webgrid-header th a", function (e) {
        e.preventDefault();
        currentGridQuery = $(this).attr('href').split("?")[1];
        $scope.loadNewGrid();
    });

    $(document).on("click", "#grid .webgrid-footer td a", function (e) {
        e.preventDefault();
        currentGridQuery = $(this).attr('href').split("?")[1];
        $scope.loadNewGrid();
    });


    $(document).on('hover', '#grid tbody tr', function () {
        $('#grid tbody tr').toggleClass('clickable');
    });

    $(document).on("click", "#grid input[name='numrows_options']", function (e) {
        e.preventDefault();
        pageParameters.rowsPerPage = parseInt($(this).attr("value"));
        $scope.loadNewGrid();
    });

    // Other events

    $(document).on("change", "#chooseSupplier", function() {
        if ($(this).val() == "") {
            return;
        }
        var url = getCurrentURL() + "/SupplierPortal/Requests/?searchName=" + currentSearchName + "&supplierId=" + $(this).val();
        window.location = url;
    });

    $(document).on("click", "#vouchPrintPDF", function () {
        var url = getCurrentURL() + "/SupplierPortal/Requests/GetVouchPrintPDF/?requestId=" + $(".selectedRequest").prop("id").split("-")[1];
        url += pageParameters.searchParameters.supplierId == 0 ? "" : "&supplierId=" + pageParameters.searchParameters.supplierId;
        window.location = url;
    });
    
    // refreshes the grid every minute
    window.setInterval($scope.refreshGrid, 600000);
    
    // Uses Angular Share Services (generalHelpers.js) to load spinner
    SharedServices.loader($scope, $('#requestLoader'));
}]);