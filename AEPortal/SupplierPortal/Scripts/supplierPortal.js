﻿var supplierDetails = {
  Id: "",
  SupplierCities: [],
  SupplierCountries: [],
  SupplierEmails: [],
  SupplierEmailSetting: null,
  SupplierOperatorIds: [],
  SupplierOpmasters: [],
  SupplierSystems: [],
  SupplierTasks: [],
  SupplierUsers: []
};

// Events

$(document).on("click", "#addSupplier", function() {
  var addSupplierRequest = {
    supplierName: $("#addSupplierName").val(),
    isManifest: $("#isManifest:checked").length !== 0 ? "True" : "False",
    isOphion: $("#isOphion:checked").length !== 0 ? "True" : "False"
  };
  addSupplier(addSupplierRequest);
});

$(document).on("change", "#chooseSupplierToEdit", function() {
  loadEditDetails($(this).val());
  supplierDetails = {
    Id: $(this).val(),
    SupplierCities: [],
    SupplierCountries: [],
    SupplierEmails: [],
    SupplierEmailSetting: null,
    SupplierOperatorIds: [],
    SupplierOpmasters: [],
    SupplierSystems: [],
    SupplierTasks: [],
    SupplierUsers: []
  };
});

$(document).on("click", ".addRow", function() {
  var newIcon = $("<div>", { "class": "removeRow glyphicon glyphicon-minus" });
  $(this).prev().append(newIcon);

  var newRow = $(this).prev().children(".emptyRow").clone();
  newRow = newRow.removeClass("emptyRow");
  $(this).prev().append(newRow);
});

$(document).on("click", ".removeRow", function() {
  $(this).next().addClass("hidden").attr("state", "Deleted");
  $(this).remove();
});

$(document).on("click", "#save", function() {
  supplierDetails.SupplierCities = [];
  supplierDetails.SupplierCountries = [];
  supplierDetails.SupplierEmails = [];
  supplierDetails.EmailSetting = null;
  supplierDetails.SupplierOperatorIds = [];
  supplierDetails.SupplierOpmasters = [];
  supplierDetails.SupplierSystems = [];
  supplierDetails.SupplierTasks = [];

  $(".editSection .supplierCities .row").each(function(index, rowHtml) {
    var row = $(rowHtml);
    if (row.hasClass("emptyRow") || row.hasClass("rowHeader")) {
      return;
    }
    var values = row.children();
    var supplierCity = {
      City: $(values[1]).val(),
      CountryCode: $(values[2]).val(),
      OperatorId: $(values[3]).val(),
      Opmaster: $(values[4]).val(),
      State: row.attr("state")
    };
    supplierDetails.SupplierCities.push(supplierCity);
  });
  $(".editSection .supplierCountries .row").each(function(index, rowHtml) {
    var row = $(rowHtml);
    if (row.hasClass("emptyRow") || row.hasClass("rowHeader")) {
      return;
    }
    var values = row.children();
    var supplierCountry = {
      CountryCode: $(values[1]).val(),
      OperatorId: $(values[2]).val(),
      Opmaster: $(values[3]).val(),
      State: row.attr("state")
    };
    supplierDetails.SupplierCountries.push(supplierCountry);
  });
  $(".editSection .supplierEmails .row").each(function(index, rowHtml) {
    var row = $(rowHtml);
    if (row.hasClass("emptyRow") || row.hasClass("rowHeader")) {
      return;
    }
    var values = row.children();
    var supplierEmail = {
      Id: $(values[0]).val(),
      Country: $(values[1]).val(),
      Email: $(values[2]).val().replace(";", ","),
      OperatorId: $(values[3]).val(),
      Opmaster: $(values[4]).val(),
      State: row.attr("state")
    };
    supplierDetails.SupplierEmails.push(supplierEmail);
  });
  $(".editSection .supplierEmailSetting .row").each(function(index, rowHtml) {
    var row = $(rowHtml);
    if (row.hasClass("emptyRow") || row.hasClass("rowHeader")) {
      return;
    }
    var values = row.children();
    var supplierEmailSetting = {
      ActionedRequests: $(values[1]).val(),
      EmailOnly: $(values[2]).val(),
      LastMinute: $(values[3]).val(),
      PendingRequests: $(values[4]).val(),
      State: row.attr("state")
    };
    supplierDetails.SupplierEmailSetting = supplierEmailSetting;
  });
  $(".editSection .supplierOperatorIds .row").each(function(index, rowHtml) {
    var row = $(rowHtml);
    if (row.hasClass("emptyRow") || row.hasClass("rowHeader")) {
      return;
    }
    var values = row.children();
    var supplierOperatorId = {
      OperatorId: $(values[1]).val(),
      State: row.attr("state")
    };
    supplierDetails.SupplierOperatorIds.push(supplierOperatorId);
  });
  $(".editSection .supplierOpmasters .row").each(function(index, rowHtml) {
    var row = $(rowHtml);
    if (row.hasClass("emptyRow") || row.hasClass("rowHeader")) {
      return;
    }
    var values = row.children();
    var supplierOpmaster = {
      Opmaster: $(values[1]).val(),
      State: row.attr("state")
    };
    supplierDetails.SupplierOpmasters.push(supplierOpmaster);
  });
  $(".editSection .supplierSystems .row").each(function(index, rowHtml) {
    var row = $(rowHtml);
    if (row.hasClass("emptyRow") || row.hasClass("rowHeader")) {
      return;
    }
    var values = row.children();
    var supplierSystem = {
      SystemCode: $(values[1]).val(),
      State: row.attr("state")
    };
    supplierDetails.SupplierSystems.push(supplierSystem);
  });
  $(".editSection .supplierTasks .row").each(function(index, rowHtml) {
    var row = $(rowHtml);
    if (row.hasClass("emptyRow") || row.hasClass("rowHeader")) {
      return;
    }
    var values = row.children();
    var supplierTask = {
      Id: $(values[0]).val(),
      Frequency: $(values[1]).val(),
      Name: $(values[2]).val(),
      NextRunDateTime: $(values[3]).val(),
      Settings: $(values[4]).val(),
      State: row.attr("state")
    };
    supplierDetails.SupplierTasks.push(supplierTask);
  });
  saveSupplierDetails();
});

$(document).on("click", "#saveUsers", function () {
  supplierDetails.SupplierUsers = [];

  $(".editSection .supplierUsers .row").each(function (index, rowHtml) {
    var row = $(rowHtml);
    if (row.hasClass("emptyRow") || row.hasClass("rowHeader")) {
        return;
    }
    var values = row.children();
    var supplierUser = {
        Id: $(values[0]).val(),
        UserIPs: $(values[1]).val() === "" ? [] : [$(values[1]).val()],
        Password: $(values[2]).val(),
        UserRoles: [$(values[3]).val()],
        Username: $(values[4]).val(),
        State: row.attr("state")
    };
    supplierDetails.SupplierUsers.push(supplierUser);
  });
  saveUsersDetails();
});
// Ajax Calls

function addSupplier(addSupplierRequest) {
  $.ajax({
    type: "POST",
    data: addSupplierRequest,
    url: getCurrentURL() + "/SupplierPortal/EditSuppliers/AddSupplier",
    success: function() {
      document.location.reload();
    },
    error: onAjaxError
  });
}

function loadEditDetails(supplierId) {
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/SupplierPortal/EditSuppliers/GetSupplierInfo/?supplierId=" + supplierId,
    dateType: "html",
    success: function(data) {
      $("#supplierDetails").html($(data + " #editSupplier").html());
    },
    error: onAjaxError
  });
}

function saveSupplierDetails() {
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/SupplierPortal/EditSuppliers/SaveSupplier",
    data: supplierDetails,
    success: function() {
      loadEditDetails(supplierDetails.Id);
    },
    error: onAjaxError
  });
}

function saveUsersDetails() {
    $.ajax({
        type: "POST",
        url: getCurrentURL() + "/SupplierPortal/EditSuppliers/SaveUsers",
        data: supplierDetails,
        success: function () {
            loadEditDetails(supplierDetails.Id);
        },
        error: onAjaxError
    });
}