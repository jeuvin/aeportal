﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SICommon.Enums;
using SICommon.Extensions;
using SICommon.LoggingOperations;
using SICommon.MaintenanceModels;
using SICommon.Models;
using SICommon.SPModels;

namespace AEPortal.SupplierPortal.Controllers {
	//[Authorize( Roles = @"AEMAINE\AE-MIS, AEMAINE\SupplierPortalAdmin" )]
	[RouteArea("SupplierPortal")]
	[RoutePrefix("EditSuppliers")]
	[Route("{action}")]
	public class EditSuppliersController : Controller {
		[Route("")]
		public ActionResult Index(SPOperation spOperation = SPOperation.SupplierOperations) {
			if (spOperation == SPOperation.SupplierOperations) {
				return EditSuppliersView();
			}
			throw new NotImplementedException();
		}

		private ActionResult EditSuppliersView() {
			var editSuppliersModel = BuildEditSuppliers();
			return View("~/SupplierPortal/Views/EditSettings/EditSuppliersHeader.cshtml", editSuppliersModel);
		}

		private EditSuppliers BuildEditSuppliers(Supplier supplier = null) {
			long supplierId = 0;

			if (supplier != null)
				supplierId = supplier.Id;

			var supplierOperationsRequest = new SupplierOperationsRequest {
				Operation = DBOperation.Read,
				Supplier = supplier,
				UserName = this.User.Identity.Name.RemoveDomainFromUsername()
			};
			var supplierOperationsResponse = TransmitToSP.TransmitAPIRequest<SupplierOperationsRequest, SupplierOperationsResponse>(supplierOperationsRequest, "Maintenance/" + SPOperation.SupplierOperations);
			if (supplierOperationsResponse == default(SupplierOperationsResponse)) {
				throw new Exception("Can not build model");
			}
			if (supplierId != 0) {
				var userOperationsRequest = new UserOperationsRequest {
					Operation = DBOperation.Read,
					SupplierId = supplier.Id,
					UserName = this.User.Identity.Name.RemoveDomainFromUsername()
				};
				var userOperationsResponse = TransmitToSP.TransmitAPIRequest<UserOperationsRequest, UserOperationsResponse>(userOperationsRequest, "Maintenance/" + SPOperation.UserOperations);
				if (userOperationsResponse == default(UserOperationsResponse)) {
					throw new Exception("Can not build model");
				}
				supplierOperationsResponse.Suppliers.First().SupplierUsers = userOperationsResponse.SupplierUsers;
			}
			return new EditSuppliers {
				Suppliers = supplierOperationsResponse.Suppliers
			};
		}

		public void AddSupplier(string supplierName, bool isManifest = true, bool isOphion = false) {
			var supplierOperationsRequest = new SupplierOperationsRequest {
				Operation = DBOperation.Create,
				Supplier = new Supplier {
					IsManifest = isManifest,
					IsOphion = isOphion,
					Name = supplierName,
				},
				UserName = this.User.Identity.Name.RemoveDomainFromUsername()
			};
			var supplierOperationsResponse = TransmitToSP.TransmitAPIRequest<SupplierOperationsRequest, SupplierOperationsResponse>(supplierOperationsRequest, "Maintenance/" + SPOperation.SupplierOperations);
			if (supplierOperationsResponse == default(SupplierOperationsResponse)) {
				throw new Exception("Add failed");
			}
		}

		public void SaveSupplier(Supplier supplier) {
			var currentSupplier = BuildEditSuppliers(supplier).Suppliers.First();
			supplier.Name = currentSupplier.Name;
			var supplierOperationRequest = new SupplierOperationsRequest {
				Operation = DBOperation.Update,
				Supplier = supplier,
				UserName = this.User.Identity.Name.RemoveDomainFromUsername()
			};
			var supplierOperationsResponse = TransmitToSP.TransmitAPIRequest<SupplierOperationsRequest, SupplierOperationsResponse>(supplierOperationRequest, "Maintenance/" + SPOperation.SupplierOperations);
			if (supplierOperationsResponse == default(SupplierOperationsResponse)) {
				throw new Exception("Save failed");
			}
		}

		public void SaveUsers(Supplier supplier) {
			var currentSupplier = BuildEditSuppliers(supplier).Suppliers.First();
			supplier.Name = currentSupplier.Name;

			if (supplier.SupplierUsers == null) {
				return;
			}
			foreach (var supplierUser in supplier.SupplierUsers) {
				if (supplierUser.State == APIModelState.Deleted) {
					continue;
				}
				supplierUser.SupplierId = supplier.Id;
				if (supplierUser.UserIPs == null) {
					supplierUser.UserIPs = new List<string>();
				}
				var userOperationsRequest = new UserOperationsRequest {
					Operation = supplierUser.State == APIModelState.Modified ? DBOperation.Update : DBOperation.Create,
					SupplierUser = supplierUser,
					UserName = this.User.Identity.Name.RemoveDomainFromUsername()
				};
				var userOperationsResponse = TransmitToSP.TransmitAPIRequest<UserOperationsRequest, UserOperationsResponse>(userOperationsRequest, "Maintenance/" + SPOperation.UserOperations);
				if (userOperationsResponse == default(UserOperationsResponse)) {
					throw new Exception("Save failed");
				}
			}
		}

		public PartialViewResult GetSupplierInfo(long supplierId) {
			var editSuppliers = BuildEditSuppliers(new Supplier() { Id = supplierId });
			return PartialView("~/SupplierPortal/Views/EditSettings/EditSupplier.cshtml", editSuppliers);
		}
	}
}