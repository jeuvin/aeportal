﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AEPortal.SupplierPortal.Helpers;
using SICommon.AEPortalModels;
using SICommon.Enums;
using SICommon.Extensions;
using SICommon.LoggingOperations;
using SICommon.Models;
using SICommon.SPModels;

namespace AEPortal.SupplierPortal.Controllers {
	//[Authorize(Roles = @"AEMAINE\AE-MIS, AEMAINE\SupplierPortalUser")]
	[RouteArea("SupplierPortal")]
	[RoutePrefix("Requests")]
	[Route("{action}")]
	public class RequestsController : Controller {
		private readonly Dictionary<long, string> supplierIdToSupplierName;
		private readonly DateTime requestStartTime = DateTime.UtcNow;

		public RequestsController() {
			var getSuppliersResponse = TransmitToSP.TransmitAPIRequest<AEPortalRequest, GetSuppliersResponse>(new AEPortalRequest { UserName = "AEPortal" }, "AEPortal/GetSuppliers");
			if (getSuppliersResponse == null) {
				return;
			}
			this.supplierIdToSupplierName = new Dictionary<long, string>();
			foreach (var supplierIdNamePair in getSuppliersResponse.SupplierIdNamePairs) {
				supplierIdToSupplierName[supplierIdNamePair.SupplierId] = supplierIdNamePair.SupplierName;
			}
		}

		public PartialViewResult GetGrid(RequestsPageParameters requestsPageParameters) {
			var requestStatus = requestsPageParameters.SearchParameters.RequestStatus;
			var gridModel = new GridModel {
				BookingRequests = GetRequestsToDisplay(requestsPageParameters.SearchParameters),
				NumRows = requestsPageParameters.RowsPerPage,
				GridSettings = DefaultSettings.requestStatusToGridSettings[requestStatus],
				RequestStatus = requestStatus,
				VoucherStatus = requestsPageParameters.SearchParameters.VoucherStatus
			};
			if (requestsPageParameters.LastRefreshDateTime.HasValue) {
				gridModel.NewRequestsCount = gridModel.BookingRequests.Count(bookingRequest => bookingRequest.SupplierResponse == null
				 ? bookingRequest.CreatedDateTime > requestsPageParameters.LastRefreshDateTime.Value
				 : bookingRequest.SupplierResponse.ResponseDateTime > requestsPageParameters.LastRefreshDateTime.Value);
			}
			var gridResult = PartialView("~/SupplierPortal/Views/Requests/_GridPartial.cshtml", gridModel);

			Task.Factory.StartNew(() => LoggingOperations.LogEndOfRequest(
				new APIRequest() {
					Browser = $"{this.Request?.Browser?.Browser} {this.Request.Browser?.Version}",
					Source = "SPSITE",
					RequestStartTime = DateTime.UtcNow
				}));
			return gridResult;
		}

		public string BuildFileForExport(SearchParameters searchParameters, ExportFormat format = ExportFormat.xlsx) {
			var exportVouchersRequest = new ExportVouchersRequest {
				ExportFormat = format,
				GetVouchersRequest = BuildGetVouchersRequest(searchParameters),
				IsAEUser = true,
				SupplierId = searchParameters.SupplierId,
				UserName = this.User.Identity.Name.RemoveDomainFromUsername()
				
			};
			var exportVoucherResponse = TransmitToSP.TransmitAPIRequest<ExportVouchersRequest, ExportVouchersResponse>(exportVouchersRequest, "ExportVouchers");

			Task.Factory.StartNew(() => LoggingOperations.LogEndOfRequest(
				new SICommon.Models.APIRequest() {
					Browser = $"{this.Request?.Browser?.Browser} {this.Request.Browser?.Version}",
					UserName = exportVouchersRequest.UserName,
					Source = "SPSITE",
					RequestStartTime = DateTime.UtcNow
				}
			));

			return !exportVoucherResponse.IsSuccess ? null : exportVoucherResponse.FileName;
		}


		public FileResult GetFileForExport(string cacheKey, ExportFormat format = ExportFormat.xlsx) {
			FileStreamResult fileResult = null;
			try {
				if (format == ExportFormat.xlsx) {
					fileResult = new FileStreamResult(new FileStream(SICommon.GeneralHelpers.EnvironmentHelpers.ExportPath + cacheKey, FileMode.Open), "application/vnd.ms-excel") {
						FileDownloadName = "Autoeurope Requests " + DateTime.UtcNow.ToSupplierPortalFormat() + ".xlsx"
					};
				} else {
					fileResult = new FileStreamResult(new FileStream(SICommon.GeneralHelpers.EnvironmentHelpers.ExportPath + cacheKey, FileMode.Open), "text/csv") {
						FileDownloadName = "Autoeurope Requests " + DateTime.UtcNow.ToSupplierPortalFormat() + ".csv"
					};
				}
			} catch (Exception xcptn) {
				LoggingOperations.LogException(new APIRequest(xcptn) {
					Browser = $"{this.Request?.Browser?.Browser} {this.Request.Browser?.Version}",
					UserName = this.User.Identity.Name
				});
			}
			Task.Factory.StartNew(() => LoggingOperations.LogEndOfRequest(
				new APIRequest() {
					Browser = $"{this.Request?.Browser?.Browser} {this.Request.Browser?.Version}",
					Source = "SPSITE",
					RequestStartTime = DateTime.UtcNow
				}
			));
			return fileResult;
		}

		private RequestsModel GetModelBySearchName(string searchName, long supplierId) {
			if (!DefaultSettings.namedSearches.ContainsKey(searchName)) {
				searchName = DefaultSettings.DEFAULT_SEARCH_NAME;
			}
			var searchParameters = DefaultSettings.namedSearches[searchName];
			searchParameters.SupplierId = supplierId;
			var requestStatus = searchParameters.RequestStatus;
			var gridSettings = DefaultSettings.requestStatusToGridSettings[requestStatus];

			var requestsToDisplay = GetRequestsToDisplay(searchParameters)?.ToList();
			return new RequestsModel {
				CarGroups = requestsToDisplay.Select(request => request.Details.CarGroup).Distinct().OrderBy(carGroup => carGroup),
				CurrentSearchName = searchName,
				DropoffLocations = requestsToDisplay.Select(request => request.Details.DropoffLocation).Distinct().OrderBy(location => location),
				PickupLocations = requestsToDisplay.Select(request => request.Details.PickupLocation).Distinct().OrderBy(location => location),
				SearchNames = DefaultSettings.namedSearches.Keys,
				SearchParameters = searchParameters,
				Suppliers = supplierIdToSupplierName,
				GridModel = new GridModel {
					BookingRequests = requestsToDisplay,
					NumRows = 20,
					GridSettings = gridSettings,
					RequestStatus = requestStatus,
					VoucherStatus = searchParameters.VoucherStatus
				}
			};
		}

		public FileResult GetVouchPrintPDF(long requestId, int supplierId = 0) {
			var getVouchPrintPDFRequest = new GetVouchPrintPDFRequest {
				RequestId = requestId,
				SupplierId = supplierId,
				UserName = this.User.Identity.Name.RemoveDomainFromUsername()
			};
			var getVouchPrintPDFResponse = TransmitToSP.TransmitAPIRequest<GetVouchPrintPDFRequest, GetVouchPrintPDFResponse>(getVouchPrintPDFRequest, "GetVouchPrintPDF");
			if (!getVouchPrintPDFResponse.IsSuccess) {
				return null;
			}
			var fileResult = new FileStreamResult(new MemoryStream(getVouchPrintPDFResponse.VoucherPDF), "application/pdf") {
				FileDownloadName = "Voucher " + getVouchPrintPDFResponse.VoucherId + ".pdf"
			};
			Task.Factory.StartNew(() => LoggingOperations.LogEndOfRequest(
				new APIRequest() {
					Browser = $"{this.Request?.Browser?.Browser} {this.Request.Browser?.Version}",
					Source = "SPSITE",
					RequestStartTime = DateTime.UtcNow
				}
			));
			return fileResult;
		}

		public PartialViewResult DoFreeTextSearch(string text) {
			if (string.IsNullOrWhiteSpace(text)) {
				return null;
			}

			var getVouchersRequest = new GetVouchersRequest {
				RequestStatus = RequestStatus.All,
				VoucherStatus = VoucherStatus.All,
				FreeSearchText = text.Trim(),
				UserName = this.User.Identity.Name.RemoveDomainFromUsername(),
				IsAEUser = true
			};

			var rqstList = TransmitToSP.TransmitAPIRequest<GetVouchersRequest, GetVouchersResponse>(getVouchersRequest, "GetVouchers");
			var requestsToDisplay = rqstList?.Vouchers?.Select(voucher => voucher.Requests.FirstOrDefault());
			var gridSettings = DefaultSettings.requestStatusToGridSettings[RequestStatus.All];
			var gridModel = new GridModel {
				BookingRequests = requestsToDisplay,
				NumRows = 20,
				GridSettings = gridSettings,
				RequestStatus = RequestStatus.All,
				VoucherStatus = VoucherStatus.All
			};
			var gridResult = PartialView("~/SupplierPortal/Views/Requests/_GridPartial.cshtml", gridModel);

			Task.Factory.StartNew(() => LoggingOperations.LogEndOfRequest(
				new APIRequest() {
					Browser = $"{this.Request?.Browser?.Browser} {this.Request.Browser?.Version}",
					Source = "SPSITE",
					RequestStartTime = DateTime.UtcNow
				}
			));
			return gridResult;
		}

		[Route("")]
		public ActionResult Index(string searchName = null, long supplierId = 0, string textSearch = "") {
			if (supplierId == 0) {
				supplierId = (int)supplierIdToSupplierName?.Keys?.First();
			}
			RequestsModel viewModel;
			if (textSearch != "") {
				viewModel = GetModelByTextSearch(textSearch);
			} else {
				viewModel = GetModelBySearchName(searchName ?? DefaultSettings.DEFAULT_SEARCH_NAME, supplierId);
			}
			Task.Factory.StartNew(() => LoggingOperations.LogEndOfRequest(
				new APIRequest() {
					Browser = $"{this.Request?.Browser?.Browser} {this.Request.Browser?.Version}",
					Source = "SPSITE",
					RequestStartTime = DateTime.UtcNow
				}
			));
			return View("~/SupplierPortal/Views/Requests/Index.cshtml", viewModel);
		}

		private RequestsModel GetModelByTextSearch(string textSearch) {
			var getVouchersRequest = new GetVouchersRequest {
				RequestStatus = RequestStatus.All,
				VoucherStatus = VoucherStatus.All,
				FreeSearchText = textSearch.Trim(),
				IsAEUser = true,
				UserName = this.User.Identity.Name.RemoveDomainFromUsername()
			};

			var searchParameters = DefaultSettings.namedSearches[DefaultSettings.DEFAULT_SEARCH_NAME];
			searchParameters.SupplierId = supplierIdToSupplierName.Keys.First();

			var rqstList = TransmitToSP.TransmitAPIRequest<GetVouchersRequest, GetVouchersResponse>(getVouchersRequest, "GetVouchers");
			var requestsToDisplay = rqstList.Vouchers.SelectMany(voucher => voucher.Requests);
			var gridSettings = DefaultSettings.requestStatusToGridSettings[RequestStatus.All];
			return new RequestsModel {
				CarGroups = requestsToDisplay.Select(request => request.Details.CarGroup).Distinct().OrderBy(carGroup => carGroup),
				CurrentSearchName = DefaultSettings.DEFAULT_SEARCH_NAME,
				DropoffLocations = requestsToDisplay.Select(request => request.Details.DropoffLocation).Distinct().OrderBy(location => location),
				PickupLocations = requestsToDisplay.Select(request => request.Details.PickupLocation).Distinct().OrderBy(location => location),
				SearchNames = DefaultSettings.namedSearches.Keys,
				SearchParameters = searchParameters,
				Suppliers = supplierIdToSupplierName,
				GridModel = new GridModel {
					BookingRequests = requestsToDisplay,
					NumRows = 20,
					GridSettings = gridSettings,
					RequestStatus = RequestStatus.All,
					VoucherStatus = VoucherStatus.All
				}
			};
		}

		private IEnumerable<BookingRequest> GetRequestsToDisplay(SearchParameters searchParameters) {
			var rqstList = GetVouchersFromAPI(searchParameters);
			if (rqstList.Vouchers == null) {
				return new List<BookingRequest>();
			}

			var requestsToDisplay = rqstList.Vouchers.SelectMany(voucher => voucher.Requests);

			if (searchParameters.RequestStatus == RequestStatus.Pending) {
				requestsToDisplay = requestsToDisplay.Where(request => request.Status == RequestStatus.Pending);
				requestsToDisplay = requestsToDisplay.GroupBy(request => request.Number).Select(x => x.FirstOrDefault());
				requestsToDisplay = requestsToDisplay.OrderBy(request => request.Details.PickupDateTime);
			}
			if (searchParameters.RequestStatus == RequestStatus.Confirmed || searchParameters.RequestStatus == RequestStatus.Denied) {
				requestsToDisplay = requestsToDisplay.Where(request => request.Status == RequestStatus.Confirmed || request.Status == RequestStatus.Denied);
				requestsToDisplay = requestsToDisplay.GroupBy(request => request.Number).Select(x => x.FirstOrDefault());
				requestsToDisplay = requestsToDisplay.Where(request => request.SupplierResponse != null);
				requestsToDisplay = requestsToDisplay.OrderByDescending(request => request.SupplierResponse.ResponseDateTime);
			}

			Task.Factory.StartNew(() => LoggingOperations.LogEndOfRequest(
				new APIRequest() {
					Browser = $"{this.Request?.Browser?.Browser} {this.Request.Browser?.Version}",
					Source = "SPSITE",
					RequestStartTime = DateTime.UtcNow
				}
			));
			return requestsToDisplay;
		}

		private SICommon.Models.GetVouchersResponse GetVouchersFromAPI(SearchParameters searchParameters) {
			var getVouchersRequest = BuildGetVouchersRequest(searchParameters);
			getVouchersRequest.Browser = $"{this.Request?.Browser?.Browser} {this.Request.Browser?.Version}";
			getVouchersRequest.UserName = this.User.Identity.Name.RemoveDomainFromUsername();
			getVouchersRequest.RequestStartTime = DateTime.UtcNow;
			return TransmitToSP.TransmitAPIRequest<GetVouchersRequest, SICommon.Models.GetVouchersResponse>(getVouchersRequest, "AEPortal/GetVouchers");
		}

		public GetVouchersRequest BuildGetVouchersRequest(SearchParameters searchParameters) {
			return new GetVouchersRequest {
				// Make sure reuqest is from AEP
				IsAEUser = true,
				RequestStartTime = DateTime.UtcNow,
				// used only in internal mode
				SupplierId = searchParameters.SupplierId,

				// required search parameters
				RequestStatus = searchParameters.RequestStatus,
				VoucherStatus = searchParameters.VoucherStatus,

				// optional search parameters
				ActionDoneBy = searchParameters.ActionDoneBy,
				CarGroup = searchParameters.CarGroup,
				ConfirmationNumber = searchParameters.ConfirmationNumber,
				DropoffBranchCodes = searchParameters.DropoffBranchCodes.ToList(),
				DropoffDateLowerBound = searchParameters.DropoffDateLowerBound ?? DateTime.UtcNow.AddYears(-10),
				DropoffDateUpperBound = searchParameters.DropoffDateUpperBound ?? DateTime.UtcNow.AddYears(10),
				FirstName = searchParameters.FirstName,
				LastName = searchParameters.LastName,
				MadeDateLowerBound = searchParameters.CreatedDateLowerBound ?? DateTime.UtcNow.AddYears(-10),
				MadeDateUpperBound = searchParameters.CreatedDateUpperBound ?? DateTime.UtcNow.AddYears(10),
				PickupBranchCodes = searchParameters.PickupBranchCodes.ToList(),
				PickupDateLowerBound = searchParameters.PickupDateLowerBound ?? DateTime.UtcNow.AddYears(-10),
				PickupDateUpperBound = searchParameters.PickupDateUpperBound ?? DateTime.UtcNow.AddYears(10),
				SystemCode = searchParameters.SystemCode,
				VoucherNumber = searchParameters.VoucherNumber
			};
		}

		public PartialViewResult GetVoucherDetails(string voucherNumber, int requestId, int supplierId = 0) {
			var voucher = GetVouchersFromAPI(new SearchParameters {
				SystemCode = (SystemCode)Enum.Parse(typeof(SystemCode), voucherNumber.Substring(0, 2)),
				VoucherNumber = Convert.ToInt32(voucherNumber.Substring(2)),
				SupplierId = supplierId
			}).Vouchers.FirstOrDefault(avoucher => avoucher.Requests.Any(request => request.RequestId == requestId));

			if (voucher?.Requests == null || !voucher.Requests.Any()) {
				this.Response.StatusCode = 500;
				return null;
			}

			// Order the requests so the most recent one is at the beginning of the list
			voucher.Requests = voucher.Requests.OrderByDescending(request => request.CreatedDateTime);

			BookingRequest requestToDisplay = null;
			if (requestId != 0) {
				requestToDisplay = voucher.Requests.FirstOrDefault(request => request.RequestId == requestId);
			}

			if (requestToDisplay == null) {
				requestToDisplay = voucher.Requests.First();
			}

			var voucherDetails = PartialView("~/SupplierPortal/Views/Requests/_VoucherDetailsPartial.cshtml", new VoucherDetailsModel {
				RequestToDisplay = requestToDisplay,
				Voucher = voucher
			});
			Task.Factory.StartNew(() => LoggingOperations.LogEndOfRequest(
				new APIRequest() {
					Browser = $"{this.Request?.Browser?.Browser} {this.Request.Browser?.Version}",
					Source = "SPSITE",
					RequestStartTime = DateTime.UtcNow
				}
			));
			return voucherDetails;
		}
	}
}