﻿var SPServices = angular.module('SPServices', ['ui.bootstrap']);
SPServices.service('SharedServices', ['$http', function ($http) {
    var service = {};

    return {
        restrict: 'A',
        loader: function (scope, elem) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function (event) {
                if (event)
                    elem.show();
                else
                    elem.hide();
            });
        }
    }
}]);

function formatDatetoDateTime(formattedDate, dateFormat) {
  try {
    var date = $.datepicker.parseDate(dateFormat, formattedDate);
    return $.datepicker.formatDate("mm/dd/yy 00:00:00", date);
  } catch (e) {
    return null;
  }
}

// Ajax Helpers

function getCurrentURL() {
  return window.location.protocol + "//" + window.location.host;
}

function onAjaxError(data) {
  $("#errorModal .modal-body h3").text(data.statusText);
  $("#errorModal").modal();
}