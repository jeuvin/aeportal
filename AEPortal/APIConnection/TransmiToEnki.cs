﻿using System.Net.Http;
using System.Text;
using AEPortal.DataLayer;
using Newtonsoft.Json;
using SICommon.Models;
using SICommon.OperationHelpers;

namespace AEPortal.APIConnection {
	internal static class TransmitToEnki {
    private static readonly HttpClientPool httpClientPool = new HttpClientPool( 5, "http://sienki.aemaine.com/" );

    private static TOperationResponse Transmit<TOperationRequest, TOperationResponse>( TOperationRequest apiRequest, string operation )
      where TOperationRequest : class
      where TOperationResponse : class {
      var httpClient = httpClientPool.GetHttpClient();

      var json = JsonConvert.SerializeObject( apiRequest );
      var response = httpClient.PostAsync( operation, new StringContent( json, Encoding.UTF8, "application/json" ) ).Result;
      if ( !response.IsSuccessStatusCode ) {
        throw new APIException( "API failure" );
      }

      httpClientPool.AddHttpClient( httpClient );
      var apiResponse = JsonConvert.DeserializeObject<TOperationResponse>( response.Content.ReadAsStringAsync().Result );
      return apiResponse;
    }

    public static APIResponse AddBackComment( AddBackCommentRequest addBackCommentRequest ) {
      return Transmit<AddBackCommentRequest, APIResponse>( addBackCommentRequest, "AddBackComment" );
    }
    public static GetBackCommentsResponse GetBackComments( GetBackCommentsRequest getBackCommentsRequest ) {
      return Transmit<GetBackCommentsRequest, GetBackCommentsResponse>( getBackCommentsRequest, "GetBackComments" );
    }

    public static GetVouchersResponse GetVouchers( GetVouchersRequest getVouchersRequest ) {
      return Transmit<GetVouchersRequest, GetVouchersResponse>( getVouchersRequest, "GetLiveVouchers" );
    }
  }
}