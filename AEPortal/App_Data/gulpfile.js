/// <binding />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
var bower = require('gulp-bower');
var concat = require('gulp-concat');
var connect = require('gulp-connect');

gulp.task('clean', function (done) {
    del(lib, done);
});

gulp.task('bower', function () {
    return bower('./components');
});

gulp.task('default', ['webserver', 'concat', 'watchStyles', 'watchScripts', 'watchCSHtml', 'watchJSON']);

gulp.task('webserver', function () {
    connect.server({
        livereload: true
    });
});

gulp.task('concat', function () {
    return gulp.src(['Manifest/Scripts/*.js'])
    .pipe(concat('libs/aeportalCombined.js'))
    .pipe(gulp.dest('.'))
    .pipe(connect.reload());
});

gulp.task('copyStyles', function () {
    gulp.src(['Content/*.css', 'Manifest/Content/*.css'])
        .pipe(gulp.dest('libs/css'))
        .pipe(connect.reload());
});

gulp.task('copyScripts', function () {
    gulp.src(['Scripts/*.js', 'Scripts/**/*.js', 'Manifest/Scripts/*.js'])
        .pipe(gulp.dest('libs/js'))
        .pipe(connect.reload());
});

gulp.task('CSHtml', function () {
    gulp.src(['Views/*.cshtml', 'Views/**/*.cshtml', 'Manifest/Views/*.cshtml', 'Manifest/Views/**/*.cshtml'])
    .pipe(connect.reload());
});

gulp.task('JSON', function () {
    gulp.src(['Manifest/Models/*.json'])
    .pipe(connect.reload());
});

gulp.task('watchStyles', function () {
    gulp.watch(['Content/*.css', 'Manifest/Content/*.css'], ['copyStyles']);
});

gulp.task('watchScripts', function () {
    gulp.watch(['Scripts/*.js', 'Scripts/**/*.js', 'Manifest/Scripts/*.js'], ['copyScripts']);
});

gulp.task('watchCSHtml', function () {
    gulp.watch(['Views/*.cshtml', 'Views/**/*.cshtml', 'Manifest/Views/*.cshtml', 'Manifest/Views/**/*.cshtml'], ['CSHtml']);
});

gulp.task('watchJSON', function () {
    gulp.watch(['Manifest/Models/*.json'], ['JSON']);
});