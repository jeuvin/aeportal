﻿using System;
using System.Collections.Concurrent;
using System.Net.Http;

namespace AEPortal.DataLayer {
  internal class HttpClientPool {
    private readonly ConcurrentQueue<HttpClient> httpClientQueue = new ConcurrentQueue<HttpClient>();
    private readonly int maxHttpClients;
    private readonly string url;

    public HttpClientPool( int maxHttpClients, string url ) {
      this.maxHttpClients = maxHttpClients;
      this.url = url;
    }

    public HttpClient GetHttpClient() {
      HttpClient httpClient = null;
      if ( this.maxHttpClients <= this.httpClientQueue.Count ) {
        this.httpClientQueue.TryDequeue( out httpClient );
      }
      return httpClient ?? GetNewHttpClient();
    }

    public void AddHttpClient( HttpClient httpClient ) {
      this.httpClientQueue.Enqueue( httpClient );
    }

    private HttpClient GetNewHttpClient() {
      return new HttpClient {
        BaseAddress = new Uri( this.url )
      };
    }
  }
}