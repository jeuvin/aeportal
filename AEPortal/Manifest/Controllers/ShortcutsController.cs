﻿using System.Web.Mvc;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.Controllers {
	//[Authorize( Roles = @"AEMAINE\AE-MIS, AEMAINE\SupplierPortalAdmin, AEMAINE\SupplierPortalUser" )]
	[RouteArea("Manifest")]
	[RoutePrefix("Shortcuts")]
	[Route("{action}")]
	public class ShortcutsController : Controller {
		[Route("")]
		public ActionResult Index() {
			return View("~/Manifest/Views/Shortcuts/Index.cshtml");
		}

		protected override void OnException(ExceptionContext filterContext) {
			filterContext.ExceptionHandled = true;
			LoggingOperations.LogException(new APIRequest(filterContext.Exception));

			// Redirect on error:
			filterContext.Result = RedirectToAction("Index", "Error");
		}
	}
}