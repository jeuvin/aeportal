﻿using System.Web.Mvc;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.Controllers {
	//[Authorize(Roles = @"AEMAINE\AE-MIS, AEMAINE\SupplierPortalAdmin")]
	[RouteArea("Manifest")]
    [RoutePrefix("General")]
    [Route("{action}")]
    public class GeneralController : Controller {
        [Route("")]
        public ActionResult Index() {
            return View("~/Manifest/Views/ErrorPages/Error.cshtml");
        }

        protected override void OnException(ExceptionContext filterContext) {
            filterContext.ExceptionHandled = true;
			LoggingOperations.LogException(new APIRequest(filterContext.Exception));

			// Redirect on error:
			filterContext.Result = RedirectToAction("Index", "Error");
        }

		public string GetColumnDefs() {
			var Response = System.Web.HttpContext.Current.Response;
			Response.Filter = new System.IO.Compression.GZipStream(Response.Filter,
										System.IO.Compression.CompressionMode.Compress);
			Response.AppendHeader("Content-Encoding", "gzip");

			string result = null;
			var columnDef = Server.MapPath("~/Manifest/Models/ColumnDefs.json");

			if (System.IO.File.Exists(columnDef)) {
				result = System.IO.File.ReadAllText(columnDef);
			}
			return result;
		}

		public string GetReportFilters() {
			var Response = System.Web.HttpContext.Current.Response;
			Response.Filter = new System.IO.Compression.GZipStream(Response.Filter,
										System.IO.Compression.CompressionMode.Compress);
			Response.AppendHeader("Content-Encoding", "gzip");

			string result = null;
			var columnDef = Server.MapPath("~/Manifest/Models/ReportFilters.json");

			if (System.IO.File.Exists(columnDef)) {
				result = System.IO.File.ReadAllText(columnDef);
			}
			return result;
		}
	}
}