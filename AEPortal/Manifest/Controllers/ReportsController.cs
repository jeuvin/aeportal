﻿using System.Web.Mvc;
using Newtonsoft.Json;
using SICommon.AEPortalModels;
using SICommon.Enums;
using SICommon.Extensions;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.Controllers {
	//[Authorize( Roles = @"AEMAINE\AE-MIS, AEMAINE\SupplierPortalAdmin" )]
	[RouteArea("Manifest")]
	[RoutePrefix("Reports")]
	[Route("{action}")]
	public class ReportsController : Controller {

		[Route("")]
		public ActionResult Index() {
			return View("~/Manifest/Views/Reports/ReportsPage.cshtml");
		}

		public string GetReportByQuery(ReportType reportType, int? page = null, string query = null) {
			switch (reportType) {
				case ReportType.EmailLog:
					return GetEmail(page, query);
				case ReportType.LoginAttempts:
					return GetLogin(page, query);
				case ReportType.UnansweredRequests:
					return GetRequest(page, query);
			}
			return null;
		}
		public string GetReport(ReportType reportType, int? page = null) {
			switch (reportType) {
				case ReportType.EmailLog:
					return GetEmail(page);
				case ReportType.LoginAttempts:
					return GetLogin(page);
				case ReportType.UnansweredRequests:
					return GetRequest(page);
			}
			return null;
		}

		private string GetEmail(int? page = null, string queryString = null) {
			var emailReport = new CreateReportRequest {
				ReportType = ReportType.EmailLog,
				UserName = this.User.Identity.Name.RemoveDomainFromUsername(),
				Page = page,
				QueryFilter = queryString == null ? null : JsonConvert.DeserializeObject<QueryFilter>(queryString)
			};
			var response = TransmitToSP.TransmitAPIRequest<CreateReportRequest, CreateReportResponse<EmailReport>>(emailReport, "CreateReport");
			return JsonConvert.SerializeObject(response);
		}

		private string GetLogin(int? page = null, string queryString = null) {
			var loginReport = new CreateReportRequest {
				ReportType = ReportType.LoginAttempts,
				UserName = this.User.Identity.Name.RemoveDomainFromUsername(),
				Page = page,
				QueryFilter = queryString == null ? null : JsonConvert.DeserializeObject<QueryFilter>(queryString)
			};
			var response = TransmitToSP.TransmitAPIRequest<CreateReportRequest, CreateReportResponse<LoginReport>>(loginReport, "CreateReport");
			return JsonConvert.SerializeObject(response);
		}

		private string GetRequest(int? page = null, string queryString = null) {
			var reportRequest = new CreateReportRequest {
				ReportType = ReportType.UnansweredRequests,
				UserName = this.User.Identity.Name.RemoveDomainFromUsername(),
				Page = page,
				QueryFilter = queryString == null ? null : JsonConvert.DeserializeObject<QueryFilter>(queryString)
			};
			var response = TransmitToSP.TransmitAPIRequest<CreateReportRequest, CreateReportResponse<RequestReport>>(reportRequest, "CreateReport");
			return JsonConvert.SerializeObject(response);
		}

		protected override void OnException(ExceptionContext filterContext) {
			filterContext.ExceptionHandled = true;
			LoggingOperations.LogException(new APIRequest(filterContext.Exception));

			// Redirect on error:
			filterContext.Result = RedirectToAction("Index", "Error");
		}
	}
}