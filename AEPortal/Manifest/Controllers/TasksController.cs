﻿using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Web.Mvc;
using AEPortal.Manifest.TaskActions;
using SICommon.AEPortalModels;
using SICommon.Enums;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.Controllers {
	//[Authorize(Roles = @"AEMAINE\AE-MIS, AEMAINE\SupplierPortalAdmin, AEMAINE\SupplierPortalUser")]
	[RouteArea("Manifest")]
    [RoutePrefix("Tasks")]
    [Route("{action}")]
    public class TasksController : Controller {
        [Route("")]
        public ActionResult Index(TaskFilterType taskFilter = TaskFilterType.Default) {
            var getAETasksRequest = new GetAETasksRequest {
                IsActive = true,
                TaskFilters = new TaskFilters(),
                TaskTypes = new List<AETaskType> { AETaskType.CreateReservation }
            };
            if (taskFilter == TaskFilterType.Default) {
                getAETasksRequest.TaskFilters.TaskStatus = AETaskStatus.Attention;
                using (var principalContext = new PrincipalContext(ContextType.Domain, "AEMAINE")) {
                    var user = UserPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, this.User.Identity.Name);

                    if (user.EmailAddress == null)
                        user.EmailAddress = string.Empty;

                    var isAU = user.EmailAddress.Contains("@driveaway");
                    if (isAU) {
                        getAETasksRequest.TaskFilters.SystemCodes = new List<SystemCode> { SystemCode.AU, SystemCode.NZ };
                    } else {
                        getAETasksRequest.TaskFilters.SystemCodes = new List<SystemCode> {
              SystemCode.CA,
              SystemCode.EU,
              SystemCode.HO,
              SystemCode.KE,
              SystemCode.LA,
              SystemCode.UK,
              SystemCode.US,
              SystemCode.ZA
            };
                    }
                }
            } else {
                getAETasksRequest.TaskFilters.SupplierType = SupplierType.EmailOnly;
            }
            return GetTasks(getAETasksRequest);
        }

		public ActionResult AddBackComment(AddCommentRequest addCommentRequest) {
            return new AddBackComment(addCommentRequest).TryExecute();
        }

        public ActionResult AddConfirmation(AddConfirmationRequest addConfirmationRequest) {
            return new AddConfirmation(addConfirmationRequest).TryExecute();
        }

        public ActionResult ChangeAssignee(long taskId, string assignee) {
            return new ChangeAssignee(assignee, taskId).TryExecute();
        }

        public ActionResult GenerateEmailTemplate(GenerateEmailTemplateRequest generateEmailTemplateRequest) {
            return new GenerateEmailTemplate(generateEmailTemplateRequest).TryExecute();
        }

        public ActionResult GetAlerts(long taskId) {
            return new GetAlerts(taskId).TryExecute();
        }

        public ActionResult GetBackComments(GetBackCommentsRequest getBackCommentsRequest) {
            return new GetBackComments(getBackCommentsRequest).TryExecute();
        }

        public ActionResult GetEmailsByVoucher(VoucherId voucherId) {
            return new GetEmails(voucherId).TryExecute();
        }

        public ActionResult GetTaskDetails(TaskDetailsRequest taskDetailsRequest) {
            return new GetTaskDetails(taskDetailsRequest).TryExecute();
        }

        public ActionResult GetTasks(GetAETasksRequest getAETasksRequest) {
            return new GetTasks(getAETasksRequest).TryExecute();
        }

        public ActionResult GetUsers() {
            return new GetUsers().TryExecute();
        }

        public ActionResult GetVouchPrintPDF(SystemName systemName, int voucherNumber) {
            return new GetVouchPrintPDF(systemName, voucherNumber).TryExecute();
        }

        public ActionResult MarkTaskAsDone(long taskId) {
            return new MarkTaskAsDone(taskId).TryExecute();
        }

        public ActionResult PerformSearch(SearchInfo searchInfo) {
            var getAETasksRequest = new GetAETasksRequest {
                FreeTextSearch = searchInfo.FreeTextSearch?.Trim(),
                IsActive = true,
                TaskFilters = searchInfo.TaskFilters,
                TaskTypes = new List<AETaskType> { AETaskType.CreateReservation }
            };
            return new GetTasks(getAETasksRequest, true).TryExecute();
        }

        public ActionResult SendCancel(SendCancelRequest sendCancelRequest) {
            return new SendCancel(sendCancelRequest).TryExecute();
        }

        public ActionResult SendEmail(SendEmailRequest sendEmailRequest) {
            return new SendEmail(sendEmailRequest).TryExecute();
        }

        public ActionResult SetAlert(SetAlertRequest setAlertRequest) {
            return new SetAlert(setAlertRequest).TryExecute();
        }

        public ActionResult SetStatus(AETaskStatus status, long taskId) {
            return new SetStatus(status, taskId).TryExecute();
        }

        protected override void OnException(ExceptionContext filterContext) {
            filterContext.ExceptionHandled = true;
            LoggingOperations.LogException(new APIRequest(filterContext.Exception));

            // Redirect on error:
            filterContext.Result = RedirectToAction("Index", "Error");
        }
		public ActionResult QuickSearchView() {
			return PartialView("~/Manifest/Views/Tasks/_quickSearch.cshtml");
		}
	}
}