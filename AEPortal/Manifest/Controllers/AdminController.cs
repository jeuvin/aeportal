﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AEPortal.Manifest.TaskActions;
using Newtonsoft.Json;
using SICommon.AEPortalModels;
using SICommon.Enums;
using SICommon.Extensions;
using SICommon.LoggingOperations;
using SICommon.Memcached;
using SICommon.Models;

namespace AEPortal.Manifest.Controllers {
	//[Authorize(Roles = @"AEMAINE\AE-MIS, AEMAINE\SupplierPortalAdmin")]
	[RouteArea("Manifest")]
    [RoutePrefix("Admin")]
    [Route("{action}")]
    public class AdminController : Controller {
        [Route("")]
        public ActionResult Index() {
            ViewBag.isAdmin = true;
            return View("~/Manifest/Views/Admin/AdminPage.cshtml");
        }

        [ChildActionOnly]
        public ActionResult NavMenuView() {
            NavMenu menu = new NavMenu();
            return PartialView("~/Manifest/Views/Admin/_NavBar.cshtml", menu.Admin);
        }

        public ActionResult UserSelectView() {
            var Users = new GetUsers();
            var userList = Users.GetUserList();
            return PartialView("~/Manifest/Views/Admin/_UserSelect.cshtml", userList);
        }

        public ActionResult GroupCodeSelectView() {
            var groupCodes = GetGroupCodeList();
            return PartialView("~/Manifest/Views/Admin/_GroupCodeSelect.cshtml", groupCodes);
        }

        public ActionResult SupplierSelectView() {
            var suppliers = GetSupplierList();
            return PartialView("~/Manifest/Views/Admin/_SupplierSelect.cshtml", suppliers);
        }

        public string AssignmentOperation(CreateAETaskRequest assignment) {
            switch (assignment.AssignmentType) {
                //CRUD Assignment Group
                case "CreateGroup":
                    return CreateAssignmentGroup(assignment);
                case "ReadGroup":
                    return GetAssignmentGroups();
                case "UpdateGroup":
                    return UpdateAssignmentGroup(assignment);
                case "DeleteGroup":
                    return DeleteAssignmentGroup(assignment);

                // CRUD User Roles
                case "CreateRole":
                    return CreateUserRoles(assignment);
                case "ReadRole":
                    return GetUserRoles();
                case "UpdateRole":
                    return UpdateUserRole(assignment);
                case "DeleteRole":
                    return DeleteUserRole(assignment);
            }
            return null;
        }

        #region User Groups
        public string GetAssignmentGroups() {
            var assignRequest = new CreateAETaskRequest {
				UserName = this.User.Identity.Name.RemoveDomainFromUsername(),
                AEAssignTaskOps = AEAssignTaskOps.ReadGroup
            };
            var response = TransmitToSP.TransmitAPIRequest<CreateAETaskRequest, CreateGenericResponse<AEAssignTask>>(assignRequest, "AutoAssignGroups");
			//TransmitToSP.TransmitAPIRequest<CreateAETaskRequest, CreateGenericResponse<AEAssignTask>>(assignRequest, "AutoAssignGroups");
			return JsonConvert.SerializeObject(response);
        }

        private string CreateAssignmentGroup(CreateAETaskRequest assignment) {
            assignment.UserName = GetAssignedUser(assignment.UserGroupId);
            assignment.AEAssignTaskOps = AEAssignTaskOps.CreateGroup;
            var response = TransmitToSP.TransmitAPIRequest<CreateAETaskRequest, CreateGenericResponse<AEAssignTask>>(assignment, "AutoAssignGroups");
            return JsonConvert.SerializeObject(response);
        }

        private string UpdateAssignmentGroup(CreateAETaskRequest assignment) {
            assignment.UserName = GetAssignedUser(assignment.UserGroupId);
            assignment.AEAssignTaskOps = AEAssignTaskOps.UpdateGroup;
            var response = TransmitToSP.TransmitAPIRequest<CreateAETaskRequest, CreateGenericResponse<AEAssignTask>>(assignment, "AutoAssignGroups");
            return JsonConvert.SerializeObject(response);
        }

        private string DeleteAssignmentGroup(CreateAETaskRequest assignment) {
            assignment.UserName = this.User.Identity.Name.RemoveDomainFromUsername();
            assignment.AEAssignTaskOps = AEAssignTaskOps.DeleteGroup;
            var response = TransmitToSP.TransmitAPIRequest<CreateAETaskRequest, CreateGenericResponse<AEAssignTask>>(assignment, "AutoAssignGroups");
            return JsonConvert.SerializeObject(response);
        }
        #endregion User Groups

        #region User Roles
        public string CreateUserRoles(CreateAETaskRequest assignment) {
            assignment.AEAssignTaskOps= AEAssignTaskOps.CreateRole;
            var response = TransmitToSP.TransmitAPIRequest<CreateAETaskRequest, CreateGenericResponse<AEAssignTask>>(assignment, "AssignUserGroups");
            return JsonConvert.SerializeObject(response);
        }
        public string GetUserRoles() {
            var response = GetUserRolesGroups();
            return JsonConvert.SerializeObject(response);
        }
        private string UpdateUserRole(CreateAETaskRequest assignment) {
            assignment.AEAssignTaskOps = AEAssignTaskOps.UpdateRole;
            
            var response = TransmitToSP.TransmitAPIRequest<CreateAETaskRequest, CreateGenericResponse<AEAssignTask>>(assignment, "AssignUserGroups");
            return JsonConvert.SerializeObject(response);
        }

        private string DeleteUserRole(CreateAETaskRequest assignment) {
            assignment.UserName = this.User.Identity.Name.RemoveDomainFromUsername();
            assignment.AEAssignTaskOps = AEAssignTaskOps.DeleteRole;

            var response = TransmitToSP.TransmitAPIRequest<CreateAETaskRequest, CreateGenericResponse<AEAssignTask>>(assignment, "AssignUserGroups");
            return JsonConvert.SerializeObject(response);
        }
        #endregion User Roles

        internal List<AEAssignTask> GetGroupCodeList() {
            var groupCodes = AccessCache.GetJSONCacheData<List<AEAssignTask>>("GetGroupCodes") ?? new List<AEAssignTask>();
  
            if (groupCodes.Count <= 1) {
                var assignRequest = new CreateAETaskRequest {
					UserName = this.User.Identity.Name.RemoveDomainFromUsername(),
                    AEAssignTaskOps = AEAssignTaskOps.GroupCodes
                };

                groupCodes = TransmitToSP.TransmitAPIRequest<CreateAETaskRequest, CreateGenericResponse<AEAssignTask>>(assignRequest, "AutoAssignGroups").Response;

                if (groupCodes.Count > 1) 
                    AccessCache.StoreJSONCacheData("GetGroupCodes", groupCodes, new TimeSpan(0, 0, 0, 30));                  
            }           
            return groupCodes;
        }

        private List<AEAssignTask> GetSupplierList() {
            var supplierList = AccessCache.GetJSONCacheData<List<AEAssignTask>>("GetSuppliers") ?? new List<AEAssignTask>();

            //if (supplierList.Count <= 1) {
                var assignRequest = new CreateAETaskRequest {
					UserName = this.User.Identity.Name.RemoveDomainFromUsername(),
                    AEAssignTaskOps = AEAssignTaskOps.Suppliers
                };

                supplierList = TransmitToSP.TransmitAPIRequest<CreateAETaskRequest, CreateGenericResponse<AEAssignTask>>(assignRequest, "AutoAssignGroups").Response;

                if (supplierList.Count > 1)
                    AccessCache.StoreJSONCacheData("GetSuppliers", supplierList, new TimeSpan(0, 0, 0, 30));
            //}
            return supplierList;
        }

        private CreateGenericResponse<AEAssignTask> GetUserRolesGroups() {
            var assignRequest = new CreateAETaskRequest {
				UserName = this.User.Identity.Name.RemoveDomainFromUsername(),
                AEAssignTaskOps = AEAssignTaskOps.ReadRole
            };
            var response = TransmitToSP.TransmitAPIRequest<CreateAETaskRequest, CreateGenericResponse<AEAssignTask>>(assignRequest, "AssignUserGroups");
            return response;
        }

        private string GetAssignedUser(long groupId) {
            string userName = null;
            try {
                var userRoles = GetUserRolesGroups();
                if (userRoles != null)
                    userName = userRoles.Response.Find(x => x.Id == groupId).UserName;
            } catch (Exception xcptn) {
				LoggingOperations.LogException(new APIRequest(xcptn));
			}
            return userName;
        }

        protected override void OnException(ExceptionContext filterContext) {
            filterContext.ExceptionHandled = true;
			LoggingOperations.LogException(new APIRequest(filterContext.Exception));

			// Redirect on error:
			filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}