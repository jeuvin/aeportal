﻿using System.Linq;
using System.Web.Mvc;
using AEPortal.APIConnection;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class GetBackComments : TaskAction {
    private readonly GetBackCommentsRequest getBackCommentsRequest;

    public GetBackComments( GetBackCommentsRequest getBackCommentsRequest ) {
      this.getBackCommentsRequest = getBackCommentsRequest;
    }

    protected override ActionResult Execute() {
      var getBackCommentsResponse = TransmitToEnki.GetBackComments( this.getBackCommentsRequest );
      var backComments = getBackCommentsResponse.BackComments.OrderBy( backComment => backComment.DateMade ).ToList();
      return new PartialViewResult {
        ViewData = new ViewDataDictionary( backComments ),
        ViewName = "~/Manifest/Views/Shared/BackComments.cshtml"
      };
    }
  }
}