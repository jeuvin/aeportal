﻿using System.Web.Mvc;
using SICommon.AEPortalModels;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class ChangeAssignee : TaskAction {
    private readonly string assignee;
    private readonly long taskId;

    public ChangeAssignee( string assignee, long taskId ) {
      this.assignee = assignee;
      this.taskId = taskId;
    }

    protected override ActionResult Execute() {
      var updateAETaskRequest = new UpdateAETaskRequest {
        AssignedTo = this.assignee,
        MarkAsFinished = false,
        TaskId = this.taskId,
        UserName = this.username
      };
      TransmitToSP.TransmitAPIRequest<UpdateAETaskRequest, APIResponse>( updateAETaskRequest, "UpdateAETask" );
      return null;
    }
  }
}