﻿using System.Web.Mvc;
using SICommon.AEPortalModels;
using SICommon.LoggingOperations;

namespace AEPortal.Manifest.TaskActions {
	internal class GetAlerts : TaskAction {
    private readonly long taskId;

    public GetAlerts( long taskId ) {
      this.taskId = taskId;
    }

    protected override ActionResult Execute() {
      var getAlertsRequest = new GetAlertsRequest {
        TaskId = this.taskId,
        UserName = this.username
      };
      var alerts = TransmitToSP.TransmitAPIRequest<GetAlertsRequest, GetAlertsResponse>( getAlertsRequest, "GetAlerts" ).TaskAlerts;
      return new PartialViewResult {
        ViewData = new ViewDataDictionary( alerts ),
        ViewName = "~/Manifest/Views/Tasks/Alerts.cshtml"
      };
    }
  }
}