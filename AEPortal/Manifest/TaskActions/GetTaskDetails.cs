﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SICommon.AEPortalModels;
using SICommon.Enums;
using SICommon.LoggingOperations;

namespace AEPortal.Manifest.TaskActions {
	internal class GetTaskDetails : TaskAction {
		private readonly TaskDetailsRequest taskDetailsRequest;

		public GetTaskDetails(TaskDetailsRequest taskDetailsRequest) {
			this.taskDetailsRequest = taskDetailsRequest;
		}

		protected override ActionResult Execute() {
			var getAETaskRequest = new GetAETasksRequest {
				IsActive = true,
				TaskTypes = new List<AETaskType> { AETaskType.CreateReservation },
				VoucherId = this.taskDetailsRequest.VoucherId,
				UserName = this.username
			};
			var getAETasksResponse = TransmitToSP.TransmitAPIRequest<GetAETasksRequest, GetAETasksResponse>(getAETaskRequest, "AEPortal/GetAETasks");

			if (getAETasksResponse == null) {
				getAETaskRequest.ErrorMessage = "Internal Server error, no results found for AETask Details";
				LoggingOperations.LogException(getAETaskRequest);
				return null;
			}

			var taskDisplays = new List<TaskDisplay>();
			foreach (var aeTask in getAETasksResponse.AETasks) {
				long requestIdToDisplay = 0;
				if (getAETasksResponse.VoucherSnapshots.Any()) {
					var requestsToDisplay = getAETasksResponse.VoucherSnapshots.Where(snapshot => snapshot.SupplierId == aeTask.Supplier.Id)
															  .SelectMany(snapshot => snapshot.Requests)
															  .OrderByDescending(request => request.CreatedDateTime);
					var requestToDisplay = requestsToDisplay.FirstOrDefault(request => request.Status == RequestStatus.Confirmed) ?? requestsToDisplay.FirstOrDefault();
					if (requestToDisplay == null) {
						requestIdToDisplay = getAETasksResponse.VoucherSnapshots.SelectMany(snapshot => snapshot.Requests)
															   .OrderByDescending(request => request.CreatedDateTime).First().Id;
					} else {
						requestIdToDisplay = requestToDisplay.Id;
					}
				}
				var voucherId = aeTask.SystemCode.ToString() + aeTask.VoucherNumber;
				taskDisplays.Add(new TaskDisplay {
					AETask = aeTask,
					RequestIdToDisplay = requestIdToDisplay,
					Snapshots = getAETasksResponse.VoucherSnapshots,
					Supplier = getAETasksResponse.VoucherIdToSupplier[voucherId],
					Voucher = getAETasksResponse.VoucherIdToLiveVoucher[voucherId]
				});
			}
			var model = new RelatedTasksDisplay {
				TaskDisplays = taskDisplays,
				TaskId = this.taskDetailsRequest.TaskId
			};
			return new PartialViewResult {
				ViewData = new ViewDataDictionary(model),
				ViewName = "~/Manifest/Views/Tasks/RelatedTasks.cshtml"
			};
		}
	}
}