﻿using System.Web.Mvc;
using SICommon.AEPortalModels;
using SICommon.Enums;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class SetStatus : TaskAction {
    private readonly AETaskStatus status;
    private readonly long taskId;
    public SetStatus( AETaskStatus status, long taskId ) {
      this.status = status;
      this.taskId = taskId;
    }

    protected override ActionResult Execute() {
      var updateAETaskRequest = new UpdateAETaskRequest {
        MarkAsFinished = false,
        TaskId = this.taskId,
        TaskStatus = this.status,
        UserName = this.username
      };
      TransmitToSP.TransmitAPIRequest<UpdateAETaskRequest, APIResponse>( updateAETaskRequest, "UpdateAETask" );
      return null;
    }
  }
}