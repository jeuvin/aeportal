﻿using System.Web.Mvc;
using SICommon.AEPortalModels;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class AddConfirmation : TaskAction {
    private readonly AddConfirmationRequest addConfirmationRequest;

    public AddConfirmation( AddConfirmationRequest addConfirmationRequest ) {
      this.addConfirmationRequest = addConfirmationRequest;
    }

    protected override ActionResult Execute() {
      var updateReservationRequest = new UpdateReservationRequest {
        Comments = (this.addConfirmationRequest.Comment ?? "").Trim(),
        ConfirmationNumber = this.addConfirmationRequest.ConfirmationNumber,
        RequestId = this.addConfirmationRequest.RequestId,
        SupplierId = this.addConfirmationRequest.SupplierId,
        UserName = this.username,
        VoucherId = new VoucherId {
          SystemCode = this.addConfirmationRequest.SystemCode,
          VoucherNumber = this.addConfirmationRequest.VoucherNumber
        },
        VoucherStatus = this.addConfirmationRequest.VoucherStatus
      };
      TransmitToSP.TransmitAPIRequest<UpdateReservationRequest, APIResponse>( updateReservationRequest, "UpdateReservation" );
      var taskDetailsRequest = new TaskDetailsRequest {
        TaskId = this.addConfirmationRequest.TaskId,
        VoucherId = new VoucherId {
          SystemCode = this.addConfirmationRequest.SystemCode,
          VoucherNumber = this.addConfirmationRequest.VoucherNumber
        }
      };
      return new GetTaskDetails( taskDetailsRequest ).TryExecute();
    }
  }
}