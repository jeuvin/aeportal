﻿using System.Web.Mvc;
using SICommon.AEPortalModels;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class SetAlert : TaskAction {
    private readonly SetAlertRequest setAlertRequest;

    public SetAlert( SetAlertRequest setAlertRequest ) {
      this.setAlertRequest = setAlertRequest;
    }

    protected override ActionResult Execute() {
      this.setAlertRequest.UserName = this.username;
      this.setAlertRequest.ShowOnDateTime = this.setAlertRequest.ShowOnDateTime.ToUniversalTime();
      TransmitToSP.TransmitAPIRequest<SetAlertRequest, APIResponse>( this.setAlertRequest, "SetAlert" );
      return this.setAlertRequest.TaskId == 0 ? null : new GetAlerts( this.setAlertRequest.TaskId ).TryExecute();
    }
  }
}