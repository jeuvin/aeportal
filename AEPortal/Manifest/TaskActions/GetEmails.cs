﻿using System.Web.Mvc;
using SICommon.AEPortalModels;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class GetEmails : TaskAction {
    private readonly VoucherId voucherId;

    public GetEmails( VoucherId voucherId ) {
      this.voucherId = voucherId;
    }

    protected override ActionResult Execute() {
      var getEmailsRequest = new GetEmailsRequest {
        UserName = this.username,
        VoucherId = this.voucherId
      };
      var getEmailsResponse = TransmitToSP.TransmitAPIRequest<GetEmailsRequest, GetEmailsResponse>( getEmailsRequest, "GetEmails" );
      return new PartialViewResult {
        ViewData = new ViewDataDictionary( getEmailsResponse.Emails ),
        ViewName = "~/Manifest/Views/Shared/Emails.cshtml"
      };
    }
  }
}