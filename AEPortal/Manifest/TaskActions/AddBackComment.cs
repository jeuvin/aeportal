﻿using System.Web.Mvc;
using AEPortal.APIConnection;
using SICommon.AEPortalModels;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class AddBackComment :TaskAction{
    private readonly AddCommentRequest addCommentRequest;

    public AddBackComment( AddCommentRequest addCommentRequest ) {
      this.addCommentRequest = addCommentRequest;
    }

    protected override ActionResult Execute() {
      TransmitToEnki.AddBackComment( new AddBackCommentRequest {
        Comment = this.addCommentRequest.Comment,
        SystemName = this.addCommentRequest.SystemName.ToString(),
        Type = "UP",
        Username = this.username,
        VoucherNumber = this.addCommentRequest.VoucherNumber
      } );
      return null;
    }
  }
}