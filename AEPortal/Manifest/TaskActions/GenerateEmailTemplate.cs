﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SICommon.AEPortalModels;
using SICommon.Enums;
using SICommon.Extensions;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class GenerateEmailTemplate : TaskAction {
    private readonly GenerateEmailTemplateRequest generateEmailTemplateRequest;

    public GenerateEmailTemplate( GenerateEmailTemplateRequest generateEmailTemplateRequest ) {
      this.generateEmailTemplateRequest = generateEmailTemplateRequest;
    }

    protected override ActionResult Execute() {
      return new JsonResult {
        Data = GenerateTemplate()
      };
    }

    private EmailTemplate GenerateTemplate() {
      var requestId = this.generateEmailTemplateRequest.RequestId;
      var templateType = this.generateEmailTemplateRequest.TemplateType;
      var getEmailDataRequest = new GetEmailDataRequest {
        RequestIds = new List<long> {requestId},
        TaskId = this.generateEmailTemplateRequest.TaskId,
        VoucherId = this.generateEmailTemplateRequest.VoucherId,
        UserName = this.username
      };
      var getEmailDataResponse = TransmitToSP.TransmitAPIRequest<GetEmailDataRequest, GetEmailDataResponse>( getEmailDataRequest, "GetEmailData" );
      var liveVoucher = getEmailDataResponse.LiveVoucher;
      var task = getEmailDataResponse.AETask;
      var emailTemplate = new EmailTemplate {
        Subject = "Auto Europe: " + task.SystemCode + task.VoucherNumber
      };
      if ( ReasonFilter.NoResponse.IsMatch( task.CreationReason ) ) {
        emailTemplate.Subject += "-Reminder";
      }
      var supplierId = task.Supplier.Id;
      if ( templateType == EmailTemplateType.SnapshotTemplate ) {
        var voucherSnapshot = getEmailDataResponse.VoucherSnapshots.First( snapshot => snapshot.Requests.Any( request => request.Id == requestId ) );
        var bookingRequest = voucherSnapshot.Requests.First( request => request.Id == requestId );
        emailTemplate.Body = BuildSnapshotTemplate( task, voucherSnapshot );
        emailTemplate.From = getEmailDataResponse.FromEmails[bookingRequest.Details.HomeCountry + voucherSnapshot.SystemCode];
        supplierId = voucherSnapshot.SupplierId;
      }
      if ( templateType == EmailTemplateType.VoucherTemplate ) {
        emailTemplate.Body = BuildVoucherTemplate( task, liveVoucher );
        emailTemplate.From = getEmailDataResponse.FromEmails[liveVoucher.HomeCountry + task.SystemCode];
      }
      if ( getEmailDataResponse.SupplierIdToSupplierEmails.ContainsKey( supplierId ) ) {
        emailTemplate.To = string.Join( ";", getEmailDataResponse.SupplierIdToSupplierEmails[supplierId] );
      }
      return emailTemplate;
    }

    private string BuildSnapshotTemplate( AETask task, VoucherSnapshot voucherSnapshot ) {
      var requestSnapshot = voucherSnapshot.Requests.First( request => request.Id == this.generateEmailTemplateRequest.RequestId );
      var bodyStr = new StringBuilder();
      bodyStr.AppendLine( "-----------------------------------------------------------------" );
      if ( voucherSnapshot.SupplierConfirmation != null && !voucherSnapshot.SupplierConfirmation.Contains( task.VoucherNumber.ToString() ) ) {
        bodyStr.AppendLine( "Reservation Number".KeyValueLine( voucherSnapshot.SupplierConfirmation ) );
      }
      bodyStr.AppendLine( "Voucher Number".KeyValueLine( task.SystemCode.ToString() + task.VoucherNumber ) );
      bodyStr.AppendLine( "Request Type".KeyValueLine( TaskTypeToRequestString( task.Type ) ) );
      bodyStr.AppendLine( "Comments".KeyValueLine( requestSnapshot.Details.Comments ?? "" ) );
      bodyStr.AppendLine( "Customer Name".KeyValueLine( requestSnapshot.Details.FullName ) );
      bodyStr.AppendLine( "Car Group".KeyValueLine( requestSnapshot.Details.CarGroup ) );
      bodyStr.AppendLine( "Pickup Location".KeyValueLine( requestSnapshot.Details.PickupLocation ) );
      bodyStr.AppendLine( "Dropoff Location".KeyValueLine( requestSnapshot.Details.DropoffLocation ) );
      bodyStr.AppendLine( "Airline Details".KeyValueLine( requestSnapshot.Details.AirlineDetails ) );
      bodyStr.AppendLine( "Pickup DateTime".KeyValueLine( requestSnapshot.Details.PickupDateTime.ToSupplierPortalFormat() ) );
      bodyStr.AppendLine( "Dropoff DateTime".KeyValueLine( requestSnapshot.Details.DropoffDateTime.ToSupplierPortalFormat() ) );
      bodyStr.AppendLine( "Rental Duration".KeyValueLine( requestSnapshot.Details.RentalDuration ) );
      bodyStr.AppendLine( "Rate Code".KeyValueLine( requestSnapshot.Details.FormattedRateCode ) );
      bodyStr.AppendLine( "Business Account".KeyValueLine( requestSnapshot.Details.FormattedBusinessAccount ) );
      bodyStr.AppendLine( "Insurance".KeyValueLine( requestSnapshot.Details.Insurance ) );
      bodyStr.AppendLine( "Special Requests".KeyValueLine( requestSnapshot.Details.FormattedSpecialRequests ) );
      bodyStr.AppendLine( "-----------------------------------------------------------------" );
      return bodyStr.ToString();
    }

    private static string BuildVoucherTemplate( AETask task, Voucher liveVoucher ) {
      var bodyStr = new StringBuilder();
      bodyStr.AppendLine( "-----------------------------------------------------------------" );
      if ( liveVoucher.ReservationNumber != null && !liveVoucher.ReservationNumber.Contains( task.VoucherNumber.ToString() ) ) {
        bodyStr.AppendLine( "Reservation Number".KeyValueLine( liveVoucher.ReservationNumber ) );
      }
      bodyStr.AppendLine( "Voucher Number".KeyValueLine( task.SystemCode.ToString() + task.VoucherNumber ) );
      bodyStr.AppendLine( "Request Type".KeyValueLine( TaskTypeToRequestString( task.Type ) ) );
      bodyStr.AppendLine( "Comments".KeyValueLine( liveVoucher.Comments ?? "" ) );
      bodyStr.AppendLine( "Customer Name".KeyValueLine( liveVoucher.FullName ) );
      bodyStr.AppendLine( "Car Group".KeyValueLine( liveVoucher.CarGroup ) );
      bodyStr.AppendLine( "Pickup Location".KeyValueLine( liveVoucher.PickupLocation ) );
      bodyStr.AppendLine( "Dropoff Location".KeyValueLine( liveVoucher.DropoffLocation ) );
      bodyStr.AppendLine( "Airline Details".KeyValueLine( liveVoucher.AirlineDetails ) );
      bodyStr.AppendLine( "Pickup DateTime".KeyValueLine( liveVoucher.PickupDateTime.ToSupplierPortalFormat() ) );
      bodyStr.AppendLine( "Dropoff DateTime".KeyValueLine( liveVoucher.DropoffDateTime.ToSupplierPortalFormat() ) );
      bodyStr.AppendLine( "Rental Duration".KeyValueLine( liveVoucher.RentalDuration ) );
      bodyStr.AppendLine( "Rate Code".KeyValueLine( liveVoucher.FormattedRateCode ) );
      bodyStr.AppendLine( "Business Account".KeyValueLine( liveVoucher.FormattedBusinessAccount ) );
      bodyStr.AppendLine( "Insurance".KeyValueLine( liveVoucher.Insurance ) );
      bodyStr.AppendLine( "Special Requests".KeyValueLine( liveVoucher.FormattedSpecialRequests ) );
      bodyStr.AppendLine( "-----------------------------------------------------------------" );
      return bodyStr.ToString();
    }

    private static string TaskTypeToRequestString( AETaskType type ) {
      if ( type == AETaskType.CreateReservation ) {
        return "New";
      }
      if ( type == AETaskType.ModifyReservation ) {
        return "Modify";
      }
      if ( type == AETaskType.CancelReservation ) {
        return "Cancel";
      }
      return "";
    }
  }
}