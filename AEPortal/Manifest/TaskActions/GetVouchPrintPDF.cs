﻿using System.IO;
using System.Web.Mvc;
using SICommon.Enums;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class GetVouchPrintPDF : TaskAction {
    private SystemName systemName;
    private int voucherNumber;

    public GetVouchPrintPDF( SystemName systemName, int voucherNumber ) {
      this.systemName = systemName;
      this.voucherNumber = voucherNumber;
    }

    protected override ActionResult Execute() {
      var getVouchPrintPDFRequest = new GetVouchPrintPDFRequest {
        SystemName = this.systemName,
        VoucherNumber = this.voucherNumber,
        UserName = this.username
      };
      var getVouchPrintPDFResponse = TransmitToSP.TransmitAPIRequest<GetVouchPrintPDFRequest, GetVouchPrintPDFResponse>( getVouchPrintPDFRequest, "GetVouchPrintPDF" );
      if ( !getVouchPrintPDFResponse.IsSuccess ) {
        return null;
      }
      var fileResult = new FileStreamResult( new MemoryStream( getVouchPrintPDFResponse.VoucherPDF ), "application/pdf" ) {
        FileDownloadName = "Voucher " + getVouchPrintPDFResponse.VoucherId + ".pdf"
      };
      return fileResult;
    }
  }
}