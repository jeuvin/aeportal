﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Web.Mvc;
using Newtonsoft.Json;
using SICommon.AEPortalModels;
using SICommon.Memcached;

namespace AEPortal.Manifest.TaskActions {
	internal class GetUsers : TaskAction {
    protected override ActionResult Execute() {
      var aeUsers = GetUserList();
      return new ContentResult {
        Content = JsonConvert.SerializeObject( aeUsers ),
        ContentType = "application/json"
      };
    }

    internal List<AEUser> GetUserList() {
            var aeUsers = AccessCache.GetJSONCacheData<List<AEUser>>("GetSupplierPortalUser") ?? new List<AEUser>();

            if (aeUsers.Count <= 1) {
                using (var principalContext = new PrincipalContext(ContextType.Domain, "AEMAINE")) {
                    var group = GroupPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, "SupplierPortalUser");
                    foreach (var member in group.Members) {
                        aeUsers.Add(new AEUser {
                            AccountName = member.SamAccountName,
                            DisplayName = member.Name
                        });
                    }
                    if (aeUsers.Count > 1) {
                        AccessCache.StoreJSONCacheData("GetSupplierPortalUser", aeUsers, new TimeSpan(0, 1, 0, 0));
                    }
                }
            }
            return aeUsers;
    }
  }
}