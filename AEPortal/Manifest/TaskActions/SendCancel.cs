﻿using System.Web.Mvc;
using SICommon.AEPortalModels;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class SendCancel : TaskAction {
    private readonly SendCancelRequest sendCancelRequest;
    public SendCancel( SendCancelRequest sendCancelRequest) {
      this.sendCancelRequest = sendCancelRequest;
    }

    protected override ActionResult Execute() {
      this.sendCancelRequest.UserName = this.username;
      var response = TransmitToSP.TransmitAPIRequest<SendCancelRequest, APIResponse>( this.sendCancelRequest, "SendCancel" );
      return null;
    }
  }
}