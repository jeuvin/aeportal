﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SICommon.Extensions;
using SICommon.LoggingOperations;
using SICommon.Models;
using SICommon.OperationHelpers;

namespace AEPortal.Manifest.TaskActions {
	internal abstract class TaskAction {
    protected readonly string username;
    private readonly DateTime requestStartDateTime = DateTime.UtcNow;

    protected TaskAction() {
      this.username = HttpContext.Current.User.Identity.Name.RemoveDomainFromUsername();
    }

    public ActionResult TryExecute() {
      try {
        var actionResult = Execute();
		var logSiteEvent = new APIRequest {
			Application = "AEPortalManifest",
			Browser = HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version,
			Event = GetType().Name,
			IsAEUser = true,
			UserAgent = HttpContext.Current.Request.UserAgent,
			UserName = this.username,
			Time = Math.Round( DateTime.UtcNow.Subtract( this.requestStartDateTime ).TotalMilliseconds, 3 )
		};
		LoggingOperations.LogEndOfRequest(logSiteEvent);
        return actionResult;
      } catch ( APIException xcptn) {
		LoggingOperations.LogException(new APIRequest(xcptn));
        return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, xcptn.Message);
      } catch ( Exception xcptn ) {
        LoggingOperations.LogException(new APIRequest(xcptn));
        return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, "Internal site error" );
      }
    }

    protected abstract ActionResult Execute();
  }
}