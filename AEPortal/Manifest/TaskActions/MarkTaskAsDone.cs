﻿using System.Web.Mvc;
using SICommon.AEPortalModels;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class MarkTaskAsDone : TaskAction {
    private readonly long taskId;

    public MarkTaskAsDone( long taskId ) {
      this.taskId = taskId;
    }

    protected override ActionResult Execute() {
      var updateAETaskRequest = new UpdateAETaskRequest {
        MarkAsFinished = true,
        TaskId = this.taskId,
        UserName = this.username
      };
      TransmitToSP.TransmitAPIRequest<UpdateAETaskRequest, APIResponse>( updateAETaskRequest, "UpdateAETask" );
      return null;
    }
  }
}