﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SICommon.AEPortalModels;
using SICommon.Enums;
using SICommon.LoggingOperations;
using SICommon.Memcached;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class GetTasks : TaskAction {
		private readonly GetAETasksRequest getAETasksRequest;
		private readonly bool isSearch;

		public GetTasks(GetAETasksRequest getAETasksRequest, bool isSearch = false) {
			this.getAETasksRequest = getAETasksRequest;
			this.isSearch = isSearch;
		}

		protected override ActionResult Execute() {
			if (!this.isSearch) {
				return new ViewResult {
					ViewData = new ViewDataDictionary(BuildModel()),
					ViewName = "~/Manifest/Views/Tasks/TasksPage.cshtml"
				};
			}
			var taskDisplayModel = BuildModel();
			if (!taskDisplayModel.Tasks.Any() && !string.IsNullOrWhiteSpace(this.getAETasksRequest.FreeTextSearch)) {
				this.getAETasksRequest.FreeTextSearch = null;
				HttpContext.Current.Response.StatusCode = 214;
				return new PartialViewResult {
					ViewData = new ViewDataDictionary(BuildModel()),
					ViewName = "~/Manifest/Views/Tasks/TasksDisplay.cshtml"
				};
			}
			return new PartialViewResult {
				ViewData = new ViewDataDictionary(taskDisplayModel),
				ViewName = "~/Manifest/Views/Tasks/TasksDisplay.cshtml"
			};
		}

		private FilterData GetFilterData() {
			var supplierOperationsRequest = new SupplierOperationsRequest {
				Operation = DBOperation.Read,
				Supplier = new Supplier {
					Id = 0
				},
				UserName = this.username
			};
			var supplierOperationsResponse = AccessCache.GetJSONCacheData<SupplierOperationsResponse>("GetSuppliers");

			if (supplierOperationsResponse == default(SupplierOperationsResponse)) {
				supplierOperationsResponse = TransmitToSP.TransmitAPIRequest<SupplierOperationsRequest, SupplierOperationsResponse>(supplierOperationsRequest, "Maintenance/" + SPOperation.SupplierOperations);
				AccessCache.StoreJSONCacheData("GetSuppliers", supplierOperationsResponse, new TimeSpan(0, 1, 0, 0));
			}
			return new FilterData {
				FilterSettings = new FilterSettings(),
				Suppliers = supplierOperationsResponse.Suppliers,
				TaskFilters = this.getAETasksRequest.TaskFilters
			};
		}

		private TasksDisplay BuildModel() {
			this.getAETasksRequest.UserName = this.username;
			var text = JsonConvert.SerializeObject(this.getAETasksRequest);
			var getAETasksResponse = TransmitToSP.TransmitAPIRequest<GetAETasksRequest, GetAETasksResponse>(this.getAETasksRequest, "AEPortal/GetAETasks");
			return new TasksDisplay {
				FilterData = GetFilterData(),
				Tasks = getAETasksResponse.AETasks
			};
		}
	}
}