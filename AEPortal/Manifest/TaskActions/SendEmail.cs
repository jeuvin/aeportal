﻿using System.Web.Mvc;
using SICommon.AEPortalModels;
using SICommon.LoggingOperations;
using SICommon.Models;

namespace AEPortal.Manifest.TaskActions {
	internal class SendEmail : TaskAction {
    private SendEmailRequest sendEmailRequest;

    public SendEmail( SendEmailRequest sendEmailRequest ) {
      this.sendEmailRequest = sendEmailRequest;
    }

    protected override ActionResult Execute() {
      this.sendEmailRequest.UserName = this.username;
      var sendEmailResponse = TransmitToSP.TransmitAPIRequest<SendEmailRequest, APIResponse>( this.sendEmailRequest, "SendEmail" );
      return null;
    }
  }
}