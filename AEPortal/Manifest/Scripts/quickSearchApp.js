﻿var quickSearchApp = angular.module('quickSearchApp', ['AEServices']);

quickSearchApp.controller('quickSearchController', ['$scope', '$http', 'SharedServices',
    function ($scope, $http, SharedServices) {
        $scope.ngQuickSearch = function () {
            var freeText = $("#quickSearch").val();
            $("#quickSearchResults").text("");
            if (freeText === "" && $("#quickSearch").val() === previousQuickSearchValue) {
                $("#quickSearch").val("");
            } else if (freeText !== "") {
                $("#activeFilters").html("");
                searchData = {};
            }
            $("#filterValues li").removeClass("active");

            $http.post(getCurrentURL() + "/Manifest/Tasks/PerformSearch", JSON.stringify({ FreeTextSearch: freeText, TaskFilters: searchData }))
            .success(function (data, status) {
                if (status === 214) {
                    $("#quickSearchResults").text("No results found");
                    $("#tasks").html($(data + " #tasks").html());
                    $("#taskArea").html("");
                }
                $("#tasks").html($(data + " #tasks").html());
                $("#taskArea").empty();
                if ($("#tasks").children().length >= 1) {
                    $("#tasks").children().first().click();
                }
            }).error(function (response, status, error) {
                onAjaxError(response);
            });
        }
        // Uses Angular Share Services (AEServices.js) to load spinner
        SharedServices.loader($scope, $('#taskLoader'));
    }]);