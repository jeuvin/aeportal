﻿var addBackComment = {
  initalizeEvents: function () {
    $("#tasksArea").on("click", ".addBackComment", function () {
      $("#backCommentModal").modal();
    });

    $("#tasksArea").on("click", "#backCommentModal .modal-body button", function () {
      var addCommentRequest = {
        Comment: $("#backCommentModal .modal-body textarea").val().trim(),
        SystemCode: $("#tasks li.active").data("systemcode"),
        VoucherNumber: $("#tasks li.active").data("vouchnum"),
      };
      addBackComment.addConfNumber(addCommentRequest);
    });

  },

  // AJAX

  addConfNumber: function (addCommentRequest) {
    $.ajax({
      type: "POST",
      url: getCurrentURL() + "/Manifest/Tasks/AddBackComment/",
      data: addCommentRequest,
      dateType: "html",
      error: onAjaxError
    });
  }
};