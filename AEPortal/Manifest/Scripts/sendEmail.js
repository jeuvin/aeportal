﻿var sendEmail = {
  initalizeEvents: function() {
    $("#tasksArea").on("click", ".showEmailModal", function() {
      $("#emailModal").modal();
    });

    $("#tasksArea").on("keydown", "#emailModal", function(e) {
      if (e.keyCode === 13) {
        $("#sendEmail").click();
      }
    });

    $("#tasksArea").on("click", "#sendEmail", function() {
      var sendEmailRequest = sendEmail.buildEmailRequest();
      sendEmail.send(sendEmailRequest);
    });

    $("#tasksArea").on("click", "#emailTemplateOptions ul li", function() {
      var emailTemplateRequest = {
        RequestId: $(".taskDisplay:not(.hidden) .requestNavigator li.active").data("requestid"),
        TaskId: $("#relatedTasks li.active").data("task-id"),
        TemplateType: $(this).children().first().data("template"),
        VoucherId: {
          SystemCode: $("#tasks li.active").data("systemcode"),
          VoucherNumber: $("#tasks li.active").data("vouchnum")
        }
      };
      sendEmail.generateEmailTemplate(emailTemplateRequest);
    });
  },

  buildEmailRequest: function() {
    return {
      Body: $("#emailBody").val(),
      CC: $("#emailCCAddress").val().split(";"),
      HomeCountry: $(".compareView:not(.hidden)").data("homecountry"),
      Subject: $("#emailSubject").val(),
      SupplierId: $(".compareView:not(.hidden)").data("supplierid"),
      SystemCode: $("#tasks li.active").data("systemcode"),
      To: $("#emailToAddress").val().split(";"),
      VoucherNumber: $("#tasks li.active").data("vouchnum")
    };
  },

  // AJAX
  generateEmailTemplate: function(emailTemplateRequest) {
    $.ajax({
      type: "POST",
      url: getCurrentURL() + "/Manifest/Tasks/generateEmailTemplate",
      data: emailTemplateRequest,
      success: function(emailTemplate) {
        $("#emailFromAddress").text(emailTemplate.From);
        $("#emailToAddress").val(emailTemplate.To);
        $("#emailBody").val(emailTemplate.Body);
        $("#emailSubject").val(emailTemplate.Subject);
      },
      error: onAjaxError
    });
  },

  send: function(sendEmailRequest) {
    $.ajax({
      type: "POST",
      url: getCurrentURL() + "/Manifest/Tasks/SendEmail",
      contentType: "application/json",
      data: JSON.stringify(sendEmailRequest),
      error: onAjaxError
    });
  }
};