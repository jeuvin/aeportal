﻿var sendCancel = {
  initalizeEvents: function() {
    $("#tasksArea").on("click", ".sendCancel", function() {
      var sendCancelRequest = {
        SupplierId: $(".compareView:not(.hidden)").data("supplierid"),
        VoucherId: {
          SystemCode: $("#tasks li.active").data("systemcode"),
          VoucherNumber: $("#tasks li.active").data("vouchnum")
        }
      };
      sendCancel.transmit(sendCancelRequest);
    });
  },

  // AJAX

  transmit: function(sendCancelRequest) {
    $.ajax({
      type: "POST",
      url: getCurrentURL() + "/Manifest/Tasks/SendCancel/",
      data: sendCancelRequest,
      dateType: "html",
      error: function (response, status, error) {
          onAjaxError(response);
      }
    });
  }
};