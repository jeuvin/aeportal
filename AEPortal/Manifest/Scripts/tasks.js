﻿var searchData = {};
var dateFormat = "dd M yy";
var previousQuickSearchValue = "";

var convertDateToLocal = function(oldDate) {
  var date = new Date(oldDate + "Z");
  return moment(date).format("M/D/YYYY HH:mm");
};

var setDateClassToLocal = function() {
  $(".date").each(function() {
    $(this).text(convertDateToLocal($(this).text().trim()));
  });
};

var setDateTimeClassToLocal = function() {
  $("#manageAlertsModal #activeAlerts .dateTimePicker").each(function() {
    $(this).val(convertDateToLocal($(this).val().trim()));
  });
};

var formatDataRows = function() {
  var rows = $(".compareView tbody td");
  for (var i = 0; i < rows.length; i += 3) {
    var currentVoucherData = $(rows[i + 1]);
    var snapshotVoucherData = $(rows[i + 2]);
    if (currentVoucherData.text().trim() !== snapshotVoucherData.text().trim()) {
      currentVoucherData.parent().addClass("bold");
      currentVoucherData.parent().addClass("danger");
    }
    if (currentVoucherData.text().trim() === "" && snapshotVoucherData.text().trim() === "") {
      currentVoucherData.parent().addClass("hidden");
    }
  }
};

var hideAddConfirmation = function(compareView) {
  if (compareView.data("status") !== "Pending") {
    $("#addConfirmation").addClass("hidden");
  } else {
    $("#addConfirmation").removeClass("hidden");
  }
};

// Keyboard shortcuts

$(document).keydown(function (e) {
  if (!e.shiftKey || !e.ctrlKey) {
    return;
  }
  e.preventDefault();
  if (e.keyCode === 65) { // a for Assign
    getUsers();
  }
  if (e.keyCode === 67) { // c for Confirm
    $("#addConfModal").modal();
  }
  if (e.keyCode === 77) { // m for Manage Alerts
    $("#manageAlertsModal").modal();
  }
});

$(document).ready(function() {
  $(".datePicker").datepicker({
    dateFormat: dateFormat
  });

  searchData = $("#activeFilters").data("taskfilters");

  // Filter Events

  $("#filterValues li").click(function() {
    if ($(this).hasClass("active")) {
      return;
    }
    $("#filterValues li").removeClass("active");
    $(this).addClass("active");
    changeFilter($(this).children("a").first().text());
  });

  $("#searchArea").on("change", "#orderByOption", function() {
    searchData["IsAscending"] = $("#orderBy .glyphicon-arrow-down").hasClass("hidden");
    searchData["OrderBy"] = $(this).val();
    performSearch("");
  });

  $("#searchArea").on("click", "#orderBy span", function() {
    $("#orderBy span.hidden").removeClass("hidden");
    $(this).addClass("hidden");
    searchData["IsAscending"] = $("#orderBy .glyphicon-arrow-down").hasClass("hidden");
    searchData["OrderBy"] = $("#orderByOption").val();
    performSearch("");
  });

  $("#searchArea").on("click", "#searchFields ul li", function() {
    var searchField = $(this).children().first();
    var customSearch = searchField.data("customsearch");
    $("#addFilterArea .filterInput").addClass("hidden");
    $("#addFilterArea select").addClass("hidden");
    $("#addFilterArea div").addClass("hidden");
    $("#addFilterArea #addFilter").addClass("hidden");
    $("#addFilterArea #filterInfo").addClass("hidden");
    if (customSearch !== "") {
      $("#" + customSearch).removeClass("hidden");
    } else {
      $("#addFilterArea input").removeClass("hidden");
    }
    if (customSearch !== "reasonFilter" && customSearch !== "supplierType" && customSearch !== "taskStatus"
      && customSearch !== "supplierSearch" && customSearch !== "systemCode") {
      $("#addFilterArea #addFilter").removeClass("hidden");
      $("#addFilterArea #filterInfo").removeClass("hidden");
    }
    $("#addFilterArea #filterInfo").text(searchField.text());
    $("#addFilterArea #filterInfo").data("key", searchField.data("key"));
    $("#addFilterArea #filterInfo").data("customsearch", customSearch);
    $("#addFilterArea #filterInfo").data("useonce", searchField.data("useonce"));
    $("#addFilterArea").removeClass("hidden");
  });

  var addFilter = function(searchVal, searchDisplayVal) {
    var customSearch = $("#addFilterArea #filterInfo").data("customsearch");
    var searchField = $("#addFilterArea #filterInfo").data("key");
    var searchName = $("#addFilterArea #filterInfo").text();
    var useOnce = $("#addFilterArea #filterInfo").data("useonce");

    var searchValue;
    var searchDisplayValue;
    if (searchVal !== "" && searchDisplayVal !== "") {
      searchValue = searchVal;
      searchDisplayValue = searchDisplayVal;
    } else if (customSearch === "dateRange") {
      searchValue = {
        LowerBound: formatDatetoDateTime($("#lowerBound").val(), dateFormat),
        UpperBound: formatDatetoDateTime($("#upperBound").val(), dateFormat)
      };
      searchDisplayValue = $("#lowerBound").val() + " To " + $("#upperBound").val();
    } else {
      searchValue = $("#addFilterArea input").val();
      searchDisplayValue = searchValue;
    }
    if (useOnce === true) {
      $("#searchFields ul li a[data-key=" + searchField + "]").parent().addClass("hidden");
    }

    if (typeof searchValue === "string") {
      searchValue = searchValue.trim();
    }
    if (useOnce === true || customSearch === "dateRange") {
      searchData[searchField] = searchValue;
    } else if (searchData[searchField] !== undefined && searchData[searchField].length !== 0) {
      searchData[searchField].push(searchValue);
    } else {
      searchData[searchField] = [searchValue];
    }

    performSearch("");
    $("#addFilterArea input").val("");
    $("#addFilterArea").addClass("hidden");
    $("#addFilterArea #addFilter").addClass("hidden");
    $("#addFilterArea #filterInfo").addClass("hidden");
    if (customSearch !== "") {
      $("#" + customSearch).addClass("hidden");
    } else {
      $("#addFilterArea input").addClass("hidden");
    }

    if (customSearch === "dateRange") {
      searchValue = "";
    }
    var activeFilter = "<div data-key=\"" + searchField +
      "\" data-customsearch=\"" + customSearch +
      "\" data-searchvalue=\"" + searchValue +
      "\" data-useonce=\"" + useOnce + "\">" +
      searchName + ": " + searchDisplayValue + "<span class=\"glyphicon glyphicon-remove\"></span>" +
      "</div>";
    $("#activeFilters").append(activeFilter);
  };

  $("#searchArea").on("click", "#addFilterArea #addFilter", function() {
    addFilter("", "");
  });

  $("#searchArea").on("click", "#addFilterArea .dropdown ul li", function() {
    var child = $($(this).children("a")[0]);
    var key = isNaN(parseInt(child.data("key"))) ? child.data("key") : parseInt(child.data("key"));
    addFilter(key, child.text());
  });

  $("#searchArea").on("click", "#activeFilters div span", function() {
    var parent = $(this).parent();
    var customSearch = parent.data("customsearch");
    var searchField = parent.data("key");
    var searchValue = parent.data("searchvalue");
    var useOnce = parent.data("useonce");

    if (useOnce === true) {
      $("#searchFields ul li a[data-key=" + searchField + "]").parent().removeClass("hidden");
    }

    if (useOnce === true || customSearch === "dateRange") {
      searchData[searchField] = null;
    } else {
      var index = searchData[searchField].indexOf(searchValue);
      if (index !== -1) {
        searchData[searchField].splice(index, 1);
      }
    }
    performSearch("");
    parent.remove();
  });

  $("#searchArea").on("keydown", "#addFilterArea input", function(e) {
    if (e.keyCode !== 13) {
      return;
    }
    $("#addFilterArea button").click();
  });

  // Task Events

  $("#tasksArea").on("click", "#tasks li", function() {
    $("#tasks li.active").removeClass("active");
    $(this).addClass("active");
    var voucherId = {
      SystemCode: $(this).data("systemcode"),
      VoucherNumber: $(this).data("vouchnum")
    };
    getTaskDetails($(this).data("task-id"), voucherId);
  });

  $("#tasksArea").on("keydown", "#tasks", function(e) {
    if (e.keyCode !== 38 && e.keyCode !== 40) {
      return;
    }
    var isUpKey = e.keyCode === 38;
    var currentSelected = $("#tasks li.active");
    if (currentSelected.length === 0) {
      $("#tasks li").first().click();
    } else if (isUpKey && currentSelected.prev().length !== 0) {
      currentSelected.prev().click();
    } else if (!isUpKey && currentSelected.next().length !== 0) {
      currentSelected.next().click();
    };
    $("#tasks").scrollTop(
      $("#tasks li.active").offset().top - $("#tasks").offset().top + $("#tasks").scrollTop()
    );
    e.preventDefault();
  });

  // Navigate between tasks

  $("#tasksArea").on("click", "#relatedTasks li:not(.active)", function() {
    var newTaskId = $(this).data("task-id");
    $(".taskDisplay:not(.hidden)").addClass("hidden");
    $(".taskDisplay[data-task-id=" + newTaskId + "]").removeClass("hidden");
    $("#relatedTasks li.active").removeClass("active");
    $(this).addClass("active");
  });

  // Navigate between snapshots

  $("#tasksArea").on("click", ".requestNavigator li:not(.active)", function() {
    var requestId = $(this).data("requestid");
    $(".taskDisplay:not(.hidden) .compareView:not(.hidden)").addClass("hidden");
    var compareView = $(".taskDisplay:not(.hidden) .compareView[data-requestid=" + requestId + "]");
    compareView.removeClass("hidden");
    hideAddConfirmation(compareView);
    $(".taskDisplay:not(.hidden) .requestNavigator li.active").removeClass("active");
    $(this).addClass("active");
  });

  // Get back comments

  $("#tasksArea").on("click", ".showCommentsModal", function() {
    var systemName = $(".taskDisplay:not(.hidden) .taskInfo").data("systemname");
    var voucherNumber = $("#tasks li.active").data("vouchnum");
    getBackComments(systemName, voucherNumber);
  });

  $("#tasksArea").on("click", "#commentsList li", function() {
    var commentId = $(this).data("commentid");
    $("#commentsArea div:not(.hidden)").addClass("hidden");
    $("#commentsArea div[data-commentid=" + commentId + "]").removeClass("hidden");
    $("#commentsList .active").removeClass("active");
    $(this).addClass("active");
  });

  $("#tasksArea").on("keydown", "#commentsModal", function(e) {
    if (e.keyCode !== 38 && e.keyCode !== 40) {
      return;
    }
    var isUpKey = e.keyCode === 38;
    var currentSelected = $("#commentsList li.active");
    if (currentSelected.length === 0) {
      $("#commentsList li").first().click();
    } else if (isUpKey && currentSelected.prev().length !== 0) {
      currentSelected.prev().click();
    } else if (!isUpKey && currentSelected.next().length !== 0) {
      currentSelected.next().click();
    };
    $("#commentsList").scrollTop(
      $("#commentsList li.active").offset().top - $("#commentsList").offset().top + $("#commentsList").scrollTop()
    );
    e.preventDefault();
  });

  // Get Emails

  $("#tasksArea").on("click", ".showEmailsModal", function() {
    var systemCode = $("#tasks li.active").data("systemcode");
    var voucherNumber = $("#tasks li.active").data("vouchnum");
    getEmails(systemCode, voucherNumber);
  });

  $("#tasksArea").on("click", "#emailsList li", function() {
    var emailId = $(this).data("emailid");
    $("#emailsArea div:not(.hidden)").addClass("hidden");
    $("#emailsArea div[data-emailid=" + emailId + "]").removeClass("hidden");
    $("#emailsList .active").removeClass("active");
    $(this).addClass("active");
  });

  $("#tasksArea").on("keydown", "#emailsModal", function(e) {
    if (e.keyCode !== 38 && e.keyCode !== 40) {
      return;
    }
    var isUpKey = e.keyCode === 38;
    var currentSelected = $("#emailsList li.active");
    if (currentSelected.length === 0) {
      $("#emailsList li").first().click();
    } else if (isUpKey && currentSelected.prev().length !== 0) {
      currentSelected.prev().click();
    } else if (!isUpKey && currentSelected.next().length !== 0) {
      currentSelected.next().click();
    };
    $("#emailsList").scrollTop(
      $("#emailsList li.active").offset().top - $("#emailsList").offset().top + $("#emailsList").scrollTop()
    );
    e.preventDefault();
  });

  // Get Vouchprint PDF
  $("#tasksArea").on("click", ".vouchPrintPDF", function() {
    var url = getCurrentURL() + "/Manifest/Tasks/GetVouchPrintPDF/?systemName=" + $(".taskDisplay:not(.hidden) .taskInfo").data("systemname") + "&voucherNumber=" + $("#tasks li.active").data("vouchnum");
    window.location = url;
  });

  // Add back comment
  addBackComment.initalizeEvents();

  // Add confirmation
  changeConfirmation.initalizeEvents();

  // Cancel events
  sendCancel.initalizeEvents();

  // Email events
  sendEmail.initalizeEvents();

  // Change Assignee
  $("#tasksArea").on("click", ".assign", getUsers);

  $("#tasksArea").on("click", "#changeAssignee", function() {
    var assignee = $("#assignModal select").val();
    var taskId = $("#relatedTasks li.active").data("task-id");
    changeAssignee(assignee, taskId);
  });

  // Mark task as done
  $("#tasksArea").on("click", ".markTaskDone", function() {
    markTaskAsDone($("#relatedTasks li.active").data("task-id"));
  });

  // Manage Alerts
  var enableAlertButton = function() {
    if ($("#addAlertDate").val() !== "" && $("#addAlertMessage").val() !== "") {
      $("#addAlert").prop("disabled", "");
    } else {
      $("#addAlert").prop("disabled", "disabled");
    }
  };
  $("#tasksArea").on("click", ".manageAlerts", function() {
    var taskId = $("#relatedTasks li.active").data("task-id");
    getTasksToManage(taskId);
  });

  $("#tasksArea").on("click", "#addAlert", function() {
    var setAlertRequest = {
      Message: $("#addAlertMessage").val(),
      ShowOnDateTime: $("#addAlertDate").val(),
      TaskId: $("#relatedTasks li.active").data("task-id")
    };
    addOrDismissAlert(setAlertRequest);
  });

  $("#tasksArea").on("change", "#addAlertDate", enableAlertButton);

  $("#tasksArea").on("change", "#addAlertMessage", enableAlertButton);

  $("#tasksArea").on("click", "#activeAlerts .taskAlert .dismissAlert", function() {
    var parent = $(this).parent();
    var setAlertRequest = {
      AlertId: parent.data("alertid"),
      IsDismissed: true,
      Message: parent.children(".alertMessage").val(),
      ShowOnDateTime: parent.children(".alertDate").val(),
      TaskId: $("#relatedTasks li.active").data("task-id")
    };
    addOrDismissAlert(setAlertRequest);
  });

  $("#tasksArea").on("keyup", "#activeAlerts .taskAlert .alertMessage", function() {
    var parent = $(this).parent();
    var setAlertRequest = {
      AlertId: parent.data("alertid"),
      Message: parent.children(".alertMessage").val(),
      ShowOnDateTime: parent.children(".alertDate").val(),
    };
    setAlert(setAlertRequest);
  });

  $("#tasksArea").on("change", "#activeAlerts .taskAlert .alertDate", function() {
    var parent = $(this).parent();
    var setAlertRequest = {
      AlertId: parent.data("alertid"),
      Message: parent.children(".alertMessage").val(),
      ShowOnDateTime: parent.children(".alertDate").val(),
    };
    setAlert(setAlertRequest);
  });

  // Set Status
  $("#tasksArea").on("click", ".taskStatusArea ul li", function() {
    var taskId = $("#relatedTasks li.active").data("task-id");
    var status = $(this).data("status");
    setStatus(taskId, status);
  });
});

// Ajax 

var changeAssignee = function(assignee, taskId) {
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/Manifest/Tasks/changeAssignee",
    data: { assignee: assignee, taskId: taskId },
    success: function() {
      $(".taskDisplay:not(.hidden) .assign").data("assignee", assignee);
      $(".taskDisplay:not(.hidden) .assign").text(assignee);
    },
    error: onAjaxError
  });
};

var changeFilter = function(taskFilter) {
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/Manifest/Tasks/",
    data: { taskFilter: taskFilter },
    success: function(data) {
      $("#activeFilters").html($(data).find("#activeFilters").html());
      searchData = $("#activeFilters").data("taskfilters");
      $("#quickSearch").val("");
      previousQuickSearchValue = "";
      $("#tasks").html($(data).find("#tasks").html());
      $("#taskArea").empty();
    },
    error: onAjaxError
  });
};

var getBackComments = function(systemName, voucherNumber) {
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/Manifest/Tasks/GetBackComments/",
    data: { SystemName: systemName, VoucherNumber: voucherNumber },
    success: function(data) {
      $("#commentsModal .modal-body").html(data);
      $("#commentsModal").modal();
    },
    error: onAjaxError
  });
};

var getEmails = function(systemCode, voucherNumber) {
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/Manifest/Tasks/GetEmailsByVoucher/",
    data: { SystemCode: systemCode, VoucherNumber: voucherNumber },
    success: function(data) {
      $("#emailsModal .modal-body").html(data);
      $("#emailsModal").modal();
    },
    error: onAjaxError
  });
};

var getTaskDetails = function(taskId, voucherId) {
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: getCurrentURL() + "/Manifest/Tasks/GetTaskDetails/",
    data: JSON.stringify({ TaskId: taskId, VoucherId: voucherId }),
    success: function(data) {
      $("#taskArea").html(data);
      formatDataRows();
      setDateClassToLocal();
      hideAddConfirmation($(".compareView:not(.hidden)"));
      $("#manageAlertsModal .dateTimePicker").datetimepicker({
        format: "n/j/Y H:i"
      });
    },
    error: function (response, status, error) {
        onAjaxError(response);
    }
  });
};

var getTasksToManage = function(taskId) {
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/Manifest/Tasks/GetAlerts/",
    data: { TaskId: taskId },
    success: function(data) {
      $("#manageAlertsModal #activeAlerts").html(data);
      setDateTimeClassToLocal();
      $("#manageAlertsModal #activeAlerts .taskAlert .dateTimePicker").datetimepicker({
        format: "n/j/Y H:i"
      });
      $("#manageAlertsModal").modal();
    },
    error: onAjaxError
  });
};

var getUsers = function() {
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/Manifest/Tasks/GetUsers",
    success: function(data) {
      $.fn.modal.Constructor.prototype.enforceFocus = function() {};
      $("#assignModal select").select2({
        data: data,
        width: "100%"
      });
      $("#assignModal select").val($("#assign").data("assignee")).trigger("change");
      $("#assignModal").modal();
    },
    error: onAjaxError
  });
};

var markTaskAsDone = function(taskId) {
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/Manifest/Tasks/MarkTaskAsDone/?taskId=" + taskId,
    dateType: "html",
    success: function () {
      var freeText = "";
      if ($("#tasks li").length > 1) {
        freeText = $("#quickSearch").val();
      }
      performSearch(freeText);
    },
    error: onAjaxError
  });
};

var performSearch = function(freeText) {
  $("#quickSearchResults").text("");
  if (freeText === "" && $("#quickSearch").val() === previousQuickSearchValue) {
    $("#quickSearch").val("");
  } else if (freeText !== "") {
    $("#activeFilters").html("");
    searchData = {};
  }
  $("#filterValues li").removeClass("active");
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/Manifest/Tasks/PerformSearch",
    contentType: "application/json",
    data: JSON.stringify({ FreeTextSearch: freeText, TaskFilters: searchData }),
    success: function(data) {
      $("#tasks").html($(data + " #tasks").html());
      $("#taskArea").empty();
      //TODO always show first element AEP-204
      if ($("#tasks").children().length >= 1) {
        $("#tasks").children().first().click();
      }
    },
    statusCode: {
      214: function(data) {
        $("#quickSearchResults").text("No results found");
        $("#tasks").html($(data + " #tasks").html());
        $("#taskArea").html("");
      }
    },
    error: function (response, status, error) {
        onAjaxError(response);
    }
  });
};

var addOrDismissAlert = function(setAlertRequest) {
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/Manifest/Tasks/SetAlert",
    contentType: "application/json",
    data: JSON.stringify(setAlertRequest),
    success: function(data) {
      $("#manageAlertsModal #activeAlerts").html(data);
      setDateTimeClassToLocal();
      $("#manageAlertsModal #activeAlerts .dateTimePicker").datetimepicker({
        format: "n/j/Y H:i"
      });
      $("#manageAlertsModal").modal();
      $("#addAlertMessage").val("");
      $("#addAlertDate").val("");
      if (setAlertRequest.IsDismissed) {
        $(".taskDisplay:not(.hidden) .showAlert[data-alertid=" + setAlertRequest.AlertId + "]").remove();
        if ($(".taskDisplay:not(.hidden) .showAlert").length === 0) {
          $(".taskDisplay:not(.hidden) .taskInfo h4").remove();
        }
      }
    },
    error: onAjaxError
  });
};

var setAlert = function(setAlertRequest) {
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/Manifest/Tasks/SetAlert",
    contentType: "application/json",
    data: JSON.stringify(setAlertRequest),
    success: function() {
      var changeSpan = $("#manageAlertsModal #activeAlerts .taskAlert[data-alertid=" + setAlertRequest.AlertId + "] span");
      changeSpan.text("Change successful").show().fadeOut(1000);
    },
    error: onAjaxError
  });
};

var setStatus = function(taskId, status) {
  $.ajax({
    type: "POST",
    url: getCurrentURL() + "/Manifest/Tasks/SetStatus",
    data: { TaskId: taskId, Status: status },
    success: function() {
      $(".taskDisplay:not(.hidden) .taskStatusDropdown").text("Status: " + status);
    },
    error: onAjaxError
  });
};