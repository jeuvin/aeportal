﻿var reportApp = angular.module('reportApp', ['AEServices', 'ui.grid', 'ui.grid.pagination', 'ui.grid.autoResize', 'ui.grid.cellNav', 'ui.grid.selection', 'ui.bootstrap']);

// Constructor
reportApp.factory('ReportData', ['$http', '$q', function ($http, $q) {
    var service = {};
    var baseUrl = getCurrentURL() + "/Manifest/Reports/";
    var _url;
    var _requestType;
    var _page;
    var _reportFilter;
    var _reportGrid;

    // Define the ReportData function
    var makeUrl = function (urlType, requestType, pageNum, query) {
        // Fetch Report Data based on URL
        switch (urlType) {
            case "Paging":
                _url = baseUrl + "GetReport/?ReportType=" + requestType + "&Page=" + pageNum;
                break;
            case "Query":
                _url = baseUrl + "GetReportByQuery/?ReportType=" + requestType + "&Page=" + pageNum + "&Query=" + query;
                break;
        };
    };

    service.getData = function (urlType, requestType, pageNum, query) {
        makeUrl(urlType, requestType, pageNum, query);
        var deffered = $q.defer();
        $http.post(_url)
            .success(function (data) {
                deffered.resolve(data);
            }).error(function (error) {
                deffered.reject();
                onAjaxError();
            });
        return deffered.promise;
    }; // getData

    var makeFilterObj = function () {
        var deffered = $q.defer();
        $http.get(getCurrentURL() + "/Manifest/General/GetReportFilters")
            .success(function (data) {
                deffered.resolve(data);
            }).error(function (error) {
                deffered.reject();
                onAjaxError();
            });
        _reportFilter = deffered.promise;
    }

    var makeGridObj = function () {
        var deffered = $q.defer();
        $http.get(getCurrentURL() + "/Manifest/General/GetColumnDefs")
            .success(function (data) {
                deffered.resolve(data);
            }).error(function(error) {
                deffered.reject();
                onAjaxError();
            });
        _reportGrid = deffered.promise;
    }

    // reportFilter {get; set - private}
    service.getFilterObj = function () {
        makeFilterObj();
        return _reportFilter;
    }

    // reportGrid {get; set - private}
    service.getGridObj = function () {
        makeGridObj();
        return _reportGrid;
    }

    // requestType {get; set}
    service.getRequestType = function () {
        return _requestType;
    }
    service.setRequestType = function (request) {
        _requestType = request;
    } // requestType

    // Page {get; set}
    service.getPage = function () {
        return _page;
    }
    service.setPage = function (page) {
        _page = page;
    } // Page

    return service;
}]); // ReportData Factory

reportApp.controller('GetDataController', ['$scope', '$http', '$timeout', '$uibModal', 'uiGridConstants', 'ReportData', 'SharedServices',
    function ($scope, $http, $timeout, $uibModal, uiGridConstants, ReportData, SharedServices) {
        $scope.term = "";
        $scope.filterObj = {};
        $scope.ColDefPromise = ReportData.getGridObj();
        var filterPromise = ReportData.getFilterObj();      
        filterPromise.then(function (data) {
            $scope.filterObj = data.ReportFilters;
            $scope.gridOptions.filterObj = data.ReportFilters;
        });
  
        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };
        $scope.pagingOptions = {
            pageNumber: 1
        };

        $scope.update = function () {
            $scope.gridData = null;
            var requestType = this.item;
            ReportData.setRequestType(requestType);
            var page = $scope.pagingOptions.pageNumber;
            var timeoutId = 0;
            var promise;

            $scope.gridApi.core.on.filterChanged($scope, function () {
                clearTimeout(timeoutId);
                var objGrid = this.grid;
                var isQuery = false;

                // Hard coded to avoid doing string contains for DateTime comlumn
                // this saves time having to check every single time since query, term 
                // and field is assign true in dateime directive already (i =1 for datetime column)
                if (objGrid.columns[1].field === 'SentDateTime' && objGrid.columns[1].filters[0].term) {
                    $scope.gridOptions.filterObj[requestType].QueryString = isQuery = true;
                }

                for (var i = 0; i < objGrid.columns.length; i++) {
                    var field = objGrid.columns[i].field;
                    var term = objGrid.columns[i].filters[0].term;                 

                    if (term) {                      
                        $scope.gridOptions.filterObj[requestType][field].FilterKey = $scope.term = term;
                        $scope.gridOptions.filterObj[requestType][field].DataType = objGrid.columns[i].colDef.type;
                        $scope.gridOptions.filterObj[requestType].QueryString = isQuery = true;
                    } else if ($scope.gridOptions.filterObj[requestType][field].FilterKey){
                        $scope.gridOptions.filterObj[requestType][field].FilterKey = "";
                        $scope.gridOptions.filterObj[requestType][field].DataType = "";
                    }
                    // makes sure that queryString is false if none are found
                    if (i == objGrid.columns.length - 1 && !isQuery)
                        $scope.gridOptions.filterObj[requestType].QueryString = false;
                }
                // fixes length error
                if ($scope.term === undefined)
                    $scope.term = "";

                if ($scope.term.length > 0 && $scope.gridOptions.filterObj[requestType].QueryString) {
                        promise = ReportData.getData("Query", requestType, page, JSON.stringify($scope.gridOptions.filterObj[requestType]));
                        $scope.gridOptions.data = [];
                        promise.then(function (data) {
                            $scope.gridOptions.data = data.Grid;
                            $scope.columnDefinitons(requestType, data.ReportFilter);
                            $scope.gridOptions.totalItems = data.Grid.length > 0 ? data.Grid[0].TotalRows : data.Grid.length;
                        });

                } else {
                    $scope.gridOptions.filterObj[requestType].QueryString = false;
                    $scope.update();
                }
                $(window).trigger('resize');
            }); //filterChange

            if (!$scope.gridOptions.filterObj[requestType].QueryString) {
                // get data for pagination
                $scope.gridOptions.data = [];
                $("#ReportGrid").addClass("hidden");
                promise = ReportData.getData("Paging", requestType, page, null);
                promise.then(function (data) {
                    // SetGridData Column Deffinition                    
                    $scope.gridOptions.data = data.Grid;
                    $scope.columnDefinitons(requestType, data.ReportFilter);
                    $scope.gridOptions.totalItems = data.Grid.length > 0 ? data.Grid[0].TotalRows : data.Grid.length;
                });
            } else if ($scope.gridOptions.filterObj[requestType].QueryString) {
                promise = ReportData.getData("Query", requestType, page, JSON.stringify($scope.gridOptions.filterObj[requestType]));
                promise.then(function (data) {
                    $scope.gridOptions.data = data.Grid;
                    $scope.columnDefinitons(requestType, data.ReportFilter);
                    $scope.gridOptions.totalItems = data.Grid.length > 0 ? data.Grid[0].TotalRows : data.Grid.length;
                });
                $(window).trigger('resize');
            }
            $("#ReportGrid").removeClass("hidden");
            $(window).trigger('resize');
        } // update

        $scope.template = {
            showInfo: function(row) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    keyboard: false,
                    controller: 'EmailModalController',
                    templateUrl: 'EmailTemplate.html',
                    resolve: {
                        selectedRow: function() {
                            return row.entity;
                        }
                    }
                });
            },
            // send user from reports to task page
            redirect: function(row) {
                var taskId;
                var voucherId;
                var result = function(row) {
                    taskId = row.entity.ID;
                    voucherId = {
                        SystemCode: row.entity.SystemCode,
                        VoucherNumber: row.entity.VoucherNumber
                    }
                };
                window.location.href = '/Manifest/Tasks';
            }
        } // Template

        $scope.gridOptions = {
            filterObj: {},
            paginationPageSize: 100,
            enableSorting: true,
            useExternalSorting: true,
            enableGridMenu: false,
            enableColumnMenus: false,
            topPager: true,
            showFooterRow: true,
            enableFiltering: true,
            useExternalPagination: true,
            enableRowSelection: true,
            noUnselect: true,
            multiSelect: false,
            enableRowHeaderSelection: false,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
                gridApi.pagination.on.paginationChanged($scope, function (newPage) {
                    $scope.pagingOptions.pageNumber = newPage;
                    ReportData.setPage(newPage);
                    $scope.update();
                }); // paginationChange

                gridApi.core.on.sortChanged($scope, (grid, sortColumns) => {
                    var requestType = ReportData.getRequestType();
                    var col = sortColumns[0];
                    var filterEnvelope = $scope.gridOptions.filterObj[requestType];
                    var hasFilter = false;

                    for (var k in filterEnvelope) {
                        if (filterEnvelope[k.toString()].FilterKey !== "")
                            hasFilter = true; 
                    }
                    
                    $scope.gridOptions.filterObj[requestType].QueryString = hasFilter;
                    $scope.gridOptions.filterObj[requestType].SortFilter.SortColumn = col ? col.field : "";
                    $scope.gridOptions.filterObj[requestType].SortFilter.SortDirection = col ? col.sort.direction : "";
                    $scope.update();
                });
            } //onRegisterApi
        }; // gridOptions 

        $scope.columnDefinitons = function (requestType, queryData) {          
            $scope.ColDefPromise.then(function (data) {
                $scope.gridData = data[requestType];
                // Set Grid Column Deffinition 
                switch (requestType) {
                    case "EmailLog":
                        $scope.gridData[2].filter['selectOptions'] = queryData.FromAddress;
                        $scope.gridData[2].filter['type'] = uiGridConstants.filter.SELECT;
                        $scope.gridData[3].filter['selectOptions'] = queryData.ToAddress;
                        $scope.gridData[3].filter['type'] = uiGridConstants.filter.SELECT;
                        $scope.gridData[4].filter['selectOptions'] = queryData.CcAddress;
                        $scope.gridData[4].filter['type'] = uiGridConstants.filter.SELECT;
                        $scope.gridData[5].filter['selectOptions'] = queryData.SupplierName;
                        $scope.gridData[5].filter['type'] = uiGridConstants.filter.SELECT;
                        $scope.gridData[6].filter['selectOptions'] = queryData.Username;
                        $scope.gridData[6].filter['type'] = uiGridConstants.filter.SELECT;
                        $scope.gridOptions.columnDefs = data[requestType];
                        break;

                    case "LoginAttempts":
                        $scope.gridData[1].filter['selectOptions'] =  queryData.FailureReason;
                        $scope.gridData[1].filter['type'] = uiGridConstants.filter.SELECT;
                        $scope.gridData[2].filter['selectOptions'] = queryData.IP;
                        $scope.gridData[2].filter['type'] = uiGridConstants.filter.SELECT;
                        $scope.gridData[3].filter['selectOptions'] = queryData.WasLoginSuccessful;
                        $scope.gridData[3].filter['type'] = uiGridConstants.filter.SELECT;
                        $scope.gridData[4].filter['selectOptions'] = queryData.Username;
                        $scope.gridData[4].filter['type'] = uiGridConstants.filter.SELECT;
                        $scope.gridOptions.columnDefs = data[requestType];
                        break;

                    case "UnansweredRequests":
                        $scope.gridData[3].filter['selectOptions'] = queryData.RequestAction;
                        $scope.gridData[3].filter['type'] = uiGridConstants.filter.SELECT;
                        $scope.gridData[5].filter['selectOptions'] = queryData.SupplierName;
                        $scope.gridData[5].filter['type'] = uiGridConstants.filter.SELECT;
                        $scope.gridData[8].filter['selectOptions'] = queryData.VoucherId;
                        $scope.gridData[8].filter['type'] = uiGridConstants.filter.SELECT;
                        $scope.gridOptions.columnDefs = data[requestType];
                        break;
                }
                
            });
        }; // columnDefs

        // Uses Angular Share Services (AEServices.js) to load spinner
        SharedServices.loader($scope, $('#reportsLoader'));
    } // function
]); // reportApp.GetDataController

reportApp.controller('EmailModalController', ['$scope', '$uibModal', '$uibModalInstance', 'selectedRow',
    function ($scope, $uibModal, $uibModalInstance, selectedRow) {
        selectedRow.SentDateTime = moment(selectedRow.SentDateTime, "YYYY-MM-DDTH:mm").format("MM-DD-YYYY H:mm");
        $scope.selectedRow = selectedRow;

        $scope.ok = function () {
            $scope.selectedRow = null;
            $uibModalInstance.dismiss('cancel');
            $uibModalInstance.close();
        };
    } //function
]); // reportApp.EmailModalController

reportApp.directive("dateRangePicker", ['uiGridConstants', 'ReportData', function (uiGridConstants, ReportData) {
    return {
        link: function (scope, element) {
            element.bind('change', function() {
                var requestType = ReportData.getRequestType();

                // check to see if field exists and a dates been picked and pass it to the isolated scope
                if (scope.col.field) {
                    if (scope.col.field) {

                        var datePicked = scope.gridOptions ? moment(scope.gridOptions, "llll").format("MM-DD-YYYY") : null;
                        scope.$root.$$childHead.filterObj[requestType][scope.col.field].FilterKey = datePicked ? datePicked : "";
                        scope.$root.$$childHead.filterObj[requestType][scope.col.field].DataType = scope.col.colDef.type ? scope.col.colDef.type : "";
                        scope.$root.$$childHead.filterObj[requestType].QueryString = true;

                        if (!datePicked) {
                            scope.$root.$$childHead.filterObj[requestType][scope.col.field].FilterKey = "";
                            scope.grid.options.filterObj[requestType][scope.col.field].FilterKey = "";
                        }
                    }
                    scope.$root.$$childHead.update();
                }
            });
        }
    }
}]);