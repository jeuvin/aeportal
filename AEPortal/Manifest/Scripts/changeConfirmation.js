﻿var changeConfirmation = {
  initalizeEvents: function() {
    $("#tasksArea").on("click", ".showConfModal", function() {
      $("#addConfModal").modal();
      if ($(".compareView:not(.hidden)").data("action") === "Cancel") {
        $("#confStatus .cancel").addClass("active");
        $("#confStatus .create").removeClass("active");
      }
    });

    $("#tasksArea").on("keydown", "#addConfModal", function(e) {
      if (e.keyCode === 13) {
        $("#submitConfirmation").click();
      }
    });

    $("#tasksArea").on("click", "#submitConfirmation", function() {
      changeConfirmation.addConfirmation();
    });

    $("#tasksArea").on("click", "#confStatus li:not(.active)", function () {
      $("#confStatus .active").removeClass("active");
      $(this).addClass("active");
    });

  },

  addConfirmation: function() {
    var addConfirmationRequest = {
      Comment: $("#addConfModal .modal-body textarea").val().trim(),
      ConfirmationNumber: $("#addConfModal .modal-body input").val().trim(),
      RequestId: $(".compareView:not(.hidden)").data("requestid"),
      SupplierId: $(".compareView:not(.hidden)").data("supplierid"),
      TaskId: $("#relatedTasks li.active").data("task-id"),
      SystemCode: $("#tasks li.active").data("systemcode"),
      SystemName: $("#taskInfo").data("systemname"),
      VoucherNumber: $("#tasks li.active").data("vouchnum"),
      VoucherStatus: $("#confStatus .active a").text()
  };
    changeConfirmation.addConfNumber(addConfirmationRequest);
  },

  // AJAX

  addConfNumber: function(addConfirmation) {
    $.ajax({
      type: "POST",
      url: getCurrentURL() + "/Manifest/Tasks/AddConfirmation/",
      data: addConfirmation,
      dateType: "html",
      success: function(data) {
        $("#taskArea").html(data);
        formatDataRows();
        setDateClassToLocal();
      },
      error: onAjaxError
    });
  }
};