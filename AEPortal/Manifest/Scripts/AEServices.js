﻿var rowObj = { "Id": null, "SystemCode": null, "SupplierId": null, "SupplierName": null, "CountryCode": null, "OfficeCode": null, "GroupCode": null, "UserName": null, "UserRole": null, "AssignedTo": null };

var AEServices = angular.module('AEServices', ['ui.bootstrap']);
AEServices.service('SharedServices', ['$rootScope', '$http', '$uibModal',  function ($rootScope, $http, $uibModal) {
    var service = {};

    return {
        restrict: 'A',
        loader: function (scope, elem) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function (event) {
                if (event)
                    elem.show();
                else
                    elem.hide();
            });
        },
        rowDialog: function (selector, template) {
            var template = (selector === 'AssignmentGroups') ? 'AsgnGrpTemplate.html' : 'UsrRowTemplate.html';
  
            switch (selector) {
                case "AssignmentGroups":
                    $uibModal.open({
                        backdrop: 'static',
                        keyboard  : false,
                        templateUrl: template,
                        controller: 'AddAssgntTemplateCtrl'
                    });
                    break;
                case "UserRoles":
                    $uibModal.open({
                        backdrop: 'static',
                        keyboard  : false,
                        templateUrl: template,
                        controller: 'AddUserTemplateCtrl'
                    });
                    break; 
            }           
        }     
    }
}]);

AEServices.controller('AddAssgntTemplateCtrl', ['$rootScope', '$uibModal', '$uibModalInstance', '$http', function ($rootScope, $uibModal, $uibModalInstance, $http) {
    $rootScope.save = function () {
        var groupIndex = document.getElementById("groupCodes").selectedIndex;
        var supplierIndex = document.getElementById("SupplierInfo").selectedIndex;
        rowObj.SystemCode = this.SystemCode === undefined ? "*" : this.SystemCode;
        rowObj.SupplierName = this.SupplierName === undefined ? "*" : this.SupplierName;
        rowObj.CountryCode = this.CountryCode === undefined ? "*" : this.CountryCode;
        rowObj.OfficeCode = this.OfficeCode === undefined ? "*" : this.OfficeCode;
        rowObj.GroupCode = this.GroupCode === undefined ? "*" : this.GroupCode;
        rowObj.UserGroupId = $('#groupCodes option')[groupIndex].id;
        rowObj.SupplierId = $('#SupplierInfo option')[supplierIndex].id;
        rowObj.AssignmentType = "CreateGroup";

        
        

        $uibModalInstance.dismiss('cancel');
        $http.post(getCurrentURL() + "/Manifest/Admin/AssignmentOperation", rowObj)
            .success(function (data) {
                // push column data info back into grid (add's the id's and supplier id to the column)
                rowObj.Id = data.Response[0].Id;
                rowObj.SupplierId = data.Response[0].SupplierId;
                $rootScope.$$childHead.gridOptions.data.push(rowObj);
                
            }).error(function (error) {
                onAjaxError();
            });
    };
    $rootScope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

AEServices.controller('AddUserTemplateCtrl', ['$rootScope', '$http', '$uibModalInstance', function ($rootScope, $http, $uibModalInstance) {
    $rootScope.save = function () {
        rowObj.GroupCode = this.Group;
        rowObj.UserName = this.UserName;
        rowObj.UserRole = this.Role;
        rowObj.AssignmentType = "CreateRole";

        $uibModalInstance.dismiss('cancel');
        $http.post(getCurrentURL() + "/Manifest/Admin/AssignmentOperation", rowObj)
            .success(function (data) {
                // push column data info back into grid (add's the id's and supplier id to the column)
                rowObj.Id = data.Response[0].Id;
                $rootScope.$$childHead.gridOptions.data.push(rowObj);
            }).error(function (error) {
                onAjaxError();
            });
    };
    $rootScope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);