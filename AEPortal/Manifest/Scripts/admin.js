﻿var adminApp = angular.module('adminApp', ['AEServices', 'ui.grid', 'ui.grid.edit', 'ui.bootstrap', 'ui.grid.autoResize']);
adminApp.controller('AutoAssignController', ['$scope', '$http', '$uibModal', 'SharedServices', function ($scope, $http, $uibModal, SharedServices) {
    $scope.gridOptions = {};
    $scope.Selector = "AssignmentGroups";
    $scope.template;
    var baseUrl = getCurrentURL() + "/Manifest/Admin/";

    // getGridColoum Def
    $http.get("/Manifest/Models/ColumnDefs.json")
            .success(function (data) {
                // assign intial grid column def
                $scope.columnDefObj = data;
                $scope.gridOptions.columnDefs = data[$('#aeNavMenu ul li.active').attr('id')];
            }).error(function (error) {
                onAjaxError();
            });

    // intial data load
    $http.post(baseUrl + 'GetAssignmentGroups')
        .success(function (data) {
            $scope.gridOptions.data = data.Response;
        }).error(function (error) {
            onAjaxError();
        });

    // Click event that changes column def
    $scope.navSelect = function ($event) {
        var newUrl;
        $scope.Selector = $event.currentTarget.id;
        $('#aeNavMenu ul li').removeClass('active');
        $('#' + $scope.Selector).addClass('active');
        $scope.gridOptions.columnDefs = $scope.columnDefObj[$scope.Selector];

        // gets GridColumn Data after menu click
        switch ($scope.Selector) {
            case "AssignmentGroups":
                newUrl = baseUrl + 'GetAssignmentGroups';
                break;
            case "UserRoles":
                newUrl = baseUrl + 'GetUserRoles';
                break;
        }
        $http.post(newUrl)
        .success(function (data) {
            $scope.gridOptions.data = data.Response;
        }).error(function (error) {
            onAjaxError();
        });
    }

    $scope.editRow = function (row) {
        $scope.template = ($scope.Selector === 'AssignmentGroups') ? 'UpdtGrpTemplate.html' : 'UptUsrTemplate.html';
        $uibModal.open({
            backdrop: 'static',
            keyboard: false,
            templateUrl: $scope.template,
            controller: 'UpdateTableCtrl',
            resolve: {
                entity: function () {
                    return row.entity;
                },
                selector: function () {
                    return $scope.Selector;
                },
                scope: function () {
                    return $scope;
                }
            } 
        });
    }

    $scope.addData = () => {
        SharedServices.rowDialog($scope.Selector, $scope.template);
    }

    // Uses Angular Share Services (AEServices.js) to load spinner
    SharedServices.loader($scope, $('#adminLoader'));
}]); // adminApp.AutoAssignController

//$uibModalInstance
adminApp.controller('UpdateTableCtrl', ['$scope', '$uibModal', '$uibModalInstance', '$http', 'entity', 'selector', 'scope', function ($scope, $uibModal, $uibModalInstance, $http, entity, selector, scope) {
    $scope.GroupSelect = function () {
        var index = document.getElementById("groupCodes").selectedIndex;
        $scope.UserGroupId = $('#groupCodes option')[index].id;
    }

    // $Scope getting passed from (this = modal) modal popup, uses ngModel to populate 
    switch (selector) {
        case "AssignmentGroups":
            $scope.SystemCode = entity.SystemCode;
            $scope.SupplierName = entity.SupplierName;
            $scope.CountryCode = entity.CountryCode;
            $scope.OfficeCode = entity.OfficeCode;
            $scope.GroupCode = entity.GroupCode;
            entity.AssignmentType = "UpdateGroup";
            break;
        case "UserRoles":
            $scope.GroupCode = entity.GroupCode;
            $scope.UserName = entity.UserName;
            $scope.UserRole = entity.UserRole;
            entity.AssignmentType = "UpdateRole";
            break;
    }

    // send to server for Update
    $scope.save = function () {
        if (entity.AssignmentType === "UpdateGroup") {
            var groupIndex = document.getElementById("groupCodes").selectedIndex;
            entity.UserGroupId = $('#groupCodes option')[groupIndex].id;
            var supplierIndex = document.getElementById("SupplierInfo").selectedIndex;           
            entity.SupplierId = $('#SupplierInfo option')[supplierIndex].id;
            entity.SupplierName = $('#SupplierInfo option')[supplierIndex].value;
        }              
        entity.GroupCode = this.GroupCode;
        entity.UserName = this.UserName;
        entity.UserRole = this.UserRole;
        

        $http.post(getCurrentURL() + "/Manifest/Admin/AssignmentOperation", entity)
            .success(function (data) {
                $uibModalInstance.close();
            }).error(function (error) {
                onAjaxError();
            });      
    };

    $scope.delete = function () {
        $scope.deleteModal = $uibModal.open({
            backdrop: 'static',
            keyboard: false,
            templateUrl: 'DeleteConfirm.html',
            controller: 'DeleteConfirmation',
            resolve: {
                rowObj: function () {
                    return entity;
                },
                scope: function () {
                    return scope;
                }, 
                selector: function () {
                    return selector;
                }
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

adminApp.controller('DeleteConfirmation', ['$scope', '$http', '$uibModalInstance', '$uibModalStack', 'rowObj', 'scope', 'selector', function ($scope, $http, $uibModalInstance, $uibModalStack, rowObj, scope, selector) {

    rowObj.AssignmentType = (selector === "AssignmentGroups") ? "DeleteGroup" : "DeleteRole";

    $scope.yes = function () {
        $uibModalStack.dismissAll();
        $http.post(getCurrentURL() + "/Manifest/Admin/AssignmentOperation", rowObj)
            .success(function (data) {
                var index = scope.$$childHead.uiGrid.data.indexOf(rowObj);
                scope.$$childHead.uiGrid.data.splice(index, 1);
            }).error(function (error) {
                onAjaxError();
            });  
    };

    $scope.no = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
