﻿using System;
using System.Web;
using SICommon.Enums;
using SICommon.Extensions;
using SICommon.LoggingOperations;
using SICommon.Memcached;
using SICommon.Models;

namespace AEPortal.Manifest.Helpers {
	public static class SupplierHelpers {
    private const string CACHE_KEY = "SupplierId|";
    private static readonly TimeSpan cacheValidFor = new TimeSpan( 1, 0, 0, 0);
    private static DateTime? lastInitalizeDateTime;
    private static readonly object lockObject = new object();

    static SupplierHelpers() {
      TryInitalizeCacheData();
    }

    private static void TryInitalizeCacheData() {
      try {
        InitalizeCacheData();
      } catch (Exception xcptn) {
        LoggingOperations.LogException(new APIRequest(xcptn));
      }
    }

    private static void InitalizeCacheData() {
      lock ( lockObject ) {
        if ( lastInitalizeDateTime.HasValue && DateTime.UtcNow.Subtract( lastInitalizeDateTime.Value ).TotalMinutes < 5 ) {
          return;
        }
        lastInitalizeDateTime = DateTime.UtcNow;
        var supplierOperationsRequest = new SupplierOperationsRequest {
          Operation = DBOperation.Read,
          Supplier = new Supplier {
            Id = 0
          },
          UserName = HttpContext.Current.User.Identity.Name.RemoveDomainFromUsername()
        };
        var supplierOperationsResponse = TransmitToSP.TransmitAPIRequest<SupplierOperationsRequest, SupplierOperationsResponse>( supplierOperationsRequest, "Maintenance/" + SPOperation.SupplierOperations );
        if ( supplierOperationsResponse == default(SupplierOperationsResponse) ) {
          throw new Exception( "Invalid API response" );
        }
        foreach ( var supplier in supplierOperationsResponse.Suppliers ) {
          AccessCache.Store( CACHE_KEY + supplier.Id, supplier.Name, cacheValidFor );
        }
      }
    }

    public static string GetSupplierName( long supplierId ) {
      var supplierName = AccessCache.Get<string>( CACHE_KEY + supplierId );
      if ( string.IsNullOrEmpty( supplierName ) ) {
        TryInitalizeCacheData();
      }
      supplierName = AccessCache.Get<string>( CACHE_KEY + supplierId );
      if ( string.IsNullOrEmpty( supplierName ) ) {
        supplierName = "Unknown";
      }
      return supplierName;
    }
  }
}