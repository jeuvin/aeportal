﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Enyim.Caching;
using SICommon.Memcached;

namespace AEPortal {
	public class MvcApplication : System.Web.HttpApplication {
		protected void Application_Start() {
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			AccessCache.memcachedClient = new MemcachedClient("enyim.com/AEPortalCache");
		}
	}
}
